
%% =====================================================
%% Types
%% =====================================================

-record(fstate, {
    regname :: atom(),
    %
    code :: binary() | atom(),
    path :: string(),
    nodes = [] :: [node()],
    groups :: [[node()]],
    %
    self :: pid(),
    ref :: reference(),
    initts :: non_neg_integer(),
    %
    state :: '$initial' | '$started' | '$loading' | '$running' | '$freezed',
    ospid :: non_neg_integer(),
    freezed_by = [] :: [{node(), reference()}],
    %
    fs_queue = [] :: [tuple()], % cooldown queue of events from file system. {{file|dir,modified|created|deleted|moved_from|moved_to},AbsPath,Timestamp}
    fs_last_dequeue_ts = 0 :: non_neg_integer(), % timestamp of last dequeue iteration to filter same timestamp events
    %
    sync_queue = [] :: [{FromNode::node(),MasterDir::string(),ToApply::[{{{dir|file,Ev::modified|created|deleted|{moved,FromMasterAbsPath::string()}},AbsPath::string(),TS::non_neg_integer()},OptsMap::map()}]}],
    %
    cache_h1 :: ets:tab(), % cache of all directories and files: AbsPath::string() => #h1{file_info::#file_info{},md5=undefined,subdir_count=undefined}
    hc :: non_neg_integer(), % hc of current cache info (only its relative data, relative path, no inode, etc)
    history :: dets:tab(),
    %
    tref :: reference(),
    timerref :: reference(),
    %
    sync_nodes = [] :: [node()], % active nodes of synchronized state
    %
    sync_pid :: undefined | pid(),
    sync_ref :: reference()
}).

-define(ESep, <<"|">>).
-define(Sep, <<"***">>).
-define(RN, <<"\n">>).

-define(DefaultMaxUserWatches,10000000).

-define(TimeoutCoolDownQueue,1000).
-define(TimeoutIdleQueue,1000).
-define(SkipModifiedTimeout,1000).

%% ===============================================
%% Modules
%% ===============================================

-define(FldUtils, fsynclib_folder_utils).

-define(Event, fsynclib_folder_event).
-define(EventApply, fsynclib_folder_event_apply).
-define(EventCache, fsynclib_folder_event_cache).
-define(EventSync, fsynclib_folder_event_sync).

-define(History, fsynclib_folder_utils_history).
-define(Sync, fsynclib_folder_sync).
-define(SyncFlow, fsynclib_folder_sync_flow).
