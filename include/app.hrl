
%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).

-define(APP, fsynclib).
-define(SUPV, fsynclib_supv).
-define(CFG, fsynclib_config).

-define(FSUPV, fsynclib_folder_supv).
-define(FSRV, fsynclib_folder_srv).
-define(PortSrv, fsynclib_folder_port_srv).

-define(Dict, fsynclib_utils_dictionary).
-define(CacheBuilder, fsynclib_utils_cache_builder).

% from basiclib
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLping, basiclib_ping).
-define(BLrpc, basiclib_rpc).
-define(BLmulticall, basiclib_multicall).
-define(BLfile, basiclib_wrapper_file).
-define(BLfilelib, basiclib_wrapper_filelib).
-define(BLfilecopier, basiclib_file_copier_srv).

%% ====================================================================
%% Define other
%% ====================================================================

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {fsync,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(?CFG:log_destination(?LOGFILE), Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).