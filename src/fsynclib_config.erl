%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.07.2020
%%% @doc get configuration parameters
%%% -------------------------------------------------------------------

-module(fsynclib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    max_user_watches/1,
    cooldown_timeout/0,
    history_deleted_keep_days/0,
    resync_interval/0,
    resync_immediately_after_fsync_result/0,
    force_sync_node/0,
    copier_bandwidth/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------
log_destination(Default) ->
    get_env('log_destination', Default).

%% --------------------
max_user_watches(Default) ->
    get_env('max_user_watches', Default).

%% --------------------
cooldown_timeout() ->
    get_env('cooldown_timeout', 1000).

%% --------------------
history_deleted_keep_days() ->
    get_env('history_deleted_keep_days', 365).

%% --------------------
resync_interval() ->
    get_env('resync_interval', 60000).

%% --------------------
resync_immediately_after_fsync_result() ->
    ?BU:to_bool(get_env('resync_immediately_after_fsync_result', true)).

%% --------------------
force_sync_node() ->
    ?BU:to_bool(get_env('force_sync_node', true)).

%% --------------------
copier_bandwidth() ->
    get_env('copier_bandwidth', 65536000).

%% ====================================================================
%% Internal functions
%% ====================================================================

get_env(Key,{fn,_}=Default) -> ?BU:get_env(?APP,Key,Default);
get_env(Key,Default) -> ?BU:get_env(?APP,Key,Default).
%get_all_env() -> ?BU:get_all_env(?APP).
%set_env(Key,Value) -> ?BU:set_env(?APP,Key,Value).
%unset_env(Key) -> ?BU:unset_env(?APP,Key).
%update_env(Opts) -> ?BU:update_env(?APP,Opts).
