%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Folder/file sync library application.
%%%   Application is setup by folders and sync nodes.
%%%      Makes cache of hash and push and get files and folders to make synchronized file system in selected folder.
%%%   Application is to run on every server of cluster one time.
%%%   Application could be setup by application:set_env, by prepare(Opts), by start(Opts), by update_opts(Opts):
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'fsync','fsynclib'}
%%%      max_user_watches
%%%         How many inode watchers should app automatically setup in system's etc
%%%         default: 10000000
%%%      cooldown_timeout
%%%         Not used.
%%%         default: 1000 ms
%%%      history_deleted_keep_days
%%%         How many days should be stored history of deleted files to guarantee it would not be restored when detached role instance with file would connect.
%%%         default: 365 days
%%%      resync_interval
%%%         How often should server sync each other in happy way.
%%%         default: 60000 ms
%%%      resync_immediately_after_fsync_result
%%%         ? Start resync just after previous result, if timer is enabled
%%%         default: true
%%%      force_sync_node
%%%         ? Initiates full sync with node if it is not in synced list or its hash differs
%%%         default: true
%%%      copier_bandwidth
%%%         Bandwidth of channel to make sync
%%%         default: 65536000 B/s
%%%      % copier_driver
%%%   Facade functions:
%%%      add_folder/4,
%%%      delete_folder/1,
%%%      get_folder_childspec/5
%%%      get_regname/1
%%% -------------------------------------------------------------------

-module(fsynclib).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0,
         stop/0]).

-export([start/2, stop/1]).

%% Facade functions
-export([
    add_folder/4,
    delete_folder/1,
    get_folder_childspec/5,
    get_regname/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ===================================================================
%% Private
%% ===================================================================

%% ====================================================================
%% Facade functions
%% ====================================================================

%% -------------------------------------------
%% Append domain supervision tree
%% -------------------------------------------
-spec add_folder(Code::binary() | atom(), Path::string(), Nodes::[node()], Groups::[[node()]]) -> {ok,Pid::pid(),RegName::atom()} | ignore | {error,Reason::term()}.
%% -------------------------------------------
add_folder(Code, Path, Nodes, Groups) ->
    RegName = get_regname(Code),
    ChildSpec = get_folder_childspec(RegName,Code,Path,Nodes,Groups),
    case ?SUPV:start_child(ChildSpec) of
        {ok,Pid} -> {ok,Pid,RegName};
        ignore -> ignore;
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Remove domain supervision tree. Used only if domain added to dmlib application's supervisor by add_domain/1
%% -------------------------------------------
-spec delete_folder(Code::binary() | atom()) -> ok | {error,Reason::term()}.
%% -------------------------------------------
delete_folder(Code) ->
    LinkName = ?FSUPV:get_linkname(Code),
    case ?SUPV:get_childspec(LinkName) of
        {ok,_} ->
            ?SUPV:terminate_child(LinkName),
            ?SUPV:delete_child(LinkName);
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Return childspec for domain's supervisor (to start under external supervisor)
%% -------------------------------------------
-spec get_folder_childspec(RegName::atom(), Code::binary() | atom(), Path::string(), Nodes::[node()], Groups::[[node()]]) -> ChildSpec::tuple().
%% -------------------------------------------
get_folder_childspec(RegName, Code, Path, Nodes, Groups) when is_atom(RegName) ->
    LinkName = ?FSUPV:get_linkname(Code),
    Opts = #{'regname' => RegName,
             'code' => Code,
             'path' => Path,
             'nodes' => Nodes,
             'groups' => Groups},
    {LinkName, {?FSUPV, start_link, [Opts]}, permanent, 1000, supervisor, [?FSUPV]}.

%% -------------------------------------------
%% Return default regname for domain's facade server
%% -------------------------------------------
-spec get_regname(Code::binary() | atom()) -> RegName::atom().
%% -------------------------------------------
get_regname(Code) when is_binary(Code); is_atom(Code) ->
    ?FSRV:get_regname(Code).

%% ====================================================================
%% Public
%% ====================================================================

%% --------------------------------------
%% Starts application
%% --------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% --------------------------------------
%% Stops application
%% --------------------------------------
stop() -> application:stop(?APP).

%% ====================================================================
%% Application callback functions
%% ====================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() ->
    % basiclib
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent).