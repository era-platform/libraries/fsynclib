%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.07.2021
%%% @doc Dictionary cache routines incapsulating work with ets or map

-module(fsynclib_utils_dictionary).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    put/2,
    delete/2,
    find/3,
    size/1,
    fold/3,
    to_list/1,
    merge/1, merge/2,
    close/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----
put(Map,{K,V}) when is_map(Map) -> Map#{K => V};
put(List,{K,V}) when is_list(List) -> [{K,V} | List];
put(Ets,{K,V}) when is_reference(Ets) -> ets:insert(Ets, {K,V}), Ets.

%% ----
delete(Map,K) when is_map(Map) -> maps:remove(Map,K);
delete(List,K) when is_list(List) -> lists:keydelete(K,1,List);
delete(Ets,K) when is_reference(Ets) -> ets:delete(Ets, K), Ets.

%% ----
find(Map,K,Default) when is_map(Map) -> maps:get(K,Map,Default);
find(List,K,Default) when is_list(List) -> ?BU:get_by_key(K,List,Default);
find(Ets,K,Default) when is_reference(Ets) ->
    case ets:lookup(Ets,K) of
        [] -> Default;
        [{K,V}] -> V
    end.

%% ----
size(Map) when is_map(Map) -> maps:size(Map);
size(List) when is_list(List) -> length(List);
size(Ets) when is_reference(Ets) -> ets:info(Ets,size).

%% ----
fold(Fun,Acc0,Map) when is_map(Map) ->
    maps:fold(fun(K,V,Acc) -> Fun({K,V},Acc) end, Acc0, Map);
fold(Fun,Acc0,List) when is_list(List) ->
    lists:foldl(Fun, Acc0, List);
fold(Fun,Acc0,Ets) when is_reference(Ets) ->
    ets:foldl(Fun, Acc0, Ets).

%% ----
to_list(Map) when is_map(Map) -> maps:to_list(Map);
to_list(List) when is_list(List) -> List;
to_list(Ets) when is_reference(Ets) -> ets:tab2list(Ets).

%% ----
merge([First|Rest]) ->
    lists:foldl(fun(Any,Acc) -> merge(Acc,Any) end, First, Rest).
%% @private
merge(Map1,Map2) when is_map(Map1), is_map(Map2) ->
    maps:merge(Map1,Map2);
merge(List1,List2) when is_list(List1), is_list(List2) ->
    lists:ukeymerge(1,lists:ukeysort(1,List2),lists:ukeysort(1,List1)); % !!!
merge(Ets1,Ets2) when is_reference(Ets1), is_reference(Ets2) ->
    ets:foldl(fun({K,V},_) -> ets:insert(Ets1,{K,V}) end, ok, Ets2),
    Ets1;
merge(Map1,Ets2) when is_map(Map1), is_reference(Ets2) ->
    ets:foldl(fun({K,V},Acc) -> Acc#{K => V} end, Map1, Ets2);
merge(List1,Ets2) when is_list(List1), is_reference(Ets2) ->
    ets:foldl(fun({K,V},Acc) -> lists:keystore(K,1,Acc,{K,V}) end, List1, Ets2);
merge(List1,Map2) when is_list(List1), is_map(Map2) ->
    maps:fold(fun(K,V,Acc) -> lists:keystore(K,1,Acc,{K,V}) end, List1, Map2);
merge(Ets1,Map2) when is_reference(Ets1), is_map(Map2) ->
    maps:fold(fun(K,V,_) -> ets:insert(Ets1,{K,V}) end, ok, Map2),
    Ets1;
merge(Map1,List2) when is_map(Map1), is_list(List2) ->
    lists:foldl(fun({K,V},Acc) -> Acc#{K => V} end, Map1, List2);
merge(Ets1,List2) when is_reference(Ets1), is_list(List2) ->
    lists:foldl(fun({K,V},_) -> ets:insert(Ets1,{K,V}) end, ok, List2),
    Ets1.

%% ----
close(Map) when is_map(Map) -> ok;
close(List) when is_list(List) -> ok;
close(Ets) when is_reference(Ets) -> ets:delete(Ets).

%% ====================================================================
%% Internal functions
%% ====================================================================
