%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.07.2021
%%% @doc

-module(fsynclib_utils_cache_builder).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    build_h1/1, build_h1/3,
    build_info_h1/1
]).

-export([build_h2/1, build_h2/2, build_h2/3]).

-export([build_h3/1, build_h3/2]).

-export([build_h4/1, build_h4/2,
         build_checksum/2]).

-export([
    refresh_h1/2,
    refresh_h2/2
]).

-export([get_info/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-include_lib("kernel/include/file.hrl").

-record(h1, {
    file_info :: #file_info{},
    subdir_count :: undefined | non_neg_integer(),
    md5 :: undefined | binary()
}).

-record(h2, {
    file_info :: #file_info{},
    subdir_count :: undefined | non_neg_integer()
}).

%% ====================================================================
%% Public functions. Build cache from disk
%% ====================================================================

%% --------------------------------------------------
%% H1: directories and all files
%% --------------------------------------------------
build_h1(Dir) ->
    filelib:ensure_dir(filename:join(Dir,".")),
    {ok,FileInfo} = file:read_file_info(Dir),
    build_h1(Dir,FileInfo,empty_hash()).

%% @private recursive
build_h1(Dir,DirInfo,Acc) ->
    Items = filelib:wildcard("*",Dir),
    Infos = lists:filtermap(fun(RelPath) ->
                                AbsPath = filename:join(Dir,RelPath),
                                case file:read_file_info(AbsPath) of
                                    {ok,FileInfo} ->
                                        {true, {AbsPath, FileInfo}};
                                    {error,enoent} ->
                                        % symlink failure
                                        false;
                                    {error,eloop} ->
                                        % symlink/mount loop failure
                                        false;
                                    {error,_} ->
                                        % other failures
                                        false
                                end end, Items),
    SubDirs = lists:filter(fun({_,#file_info{type=T}}) -> T=='directory' end, Infos),
    Acc1 = ?Dict:put(Acc,{Dir,#h1{file_info = DirInfo,
                                  subdir_count = undefined}}), % length(SubDirs)
    Acc2 = lists:foldl(fun({PathF,#file_info{type='regular'}=FileInfoF},AccF) ->
                                ?Dict:put(AccF, {PathF,#h1{file_info = FileInfoF,
                                                           md5 = md5(PathF)}});
                          (_,AccF) -> AccF
                       end, Acc1, Infos),
    lists:foldl(fun({SubDirF,FileInfoF},AccF) -> build_h1(SubDirF,FileInfoF,AccF) end, Acc2, SubDirs).

%%
build_info_h1(AbsPath) ->
    case file:read_file_info(AbsPath) of
        {ok,#file_info{type='regular'}=FileInfo} ->
            #h1{file_info=FileInfo,
                md5 = md5(AbsPath)};
        {ok,#file_info{type='directory'}=FileInfo} ->
            #h1{file_info=FileInfo,
                subdir_count=undefined}; % 0
        {error,enoent} ->
            % symlink failure
            false;
        {error,eloop} ->
            % symlink/mount loop failure
            false;
        {error,_} ->
            % other failures
            false
    end.

%% --------------------------------------------------
%% H2: only directories
%% --------------------------------------------------
build_h2(Dir) ->
    filelib:ensure_dir(filename:join(Dir,".")),
    {ok,FileInfo} = file:read_file_info(Dir),
    build_h2(Dir,FileInfo,empty_hash()).

build_h2(_Dir,FromH1) ->
    ?Dict:fold(fun({K,#h1{file_info=#file_info{type='directory'}=DirInfo,subdir_count=SubdirCnt}},Acc) ->
                       ?Dict:put(Acc,{K,#h2{file_info=DirInfo,subdir_count=SubdirCnt}})
                end, empty_hash(), FromH1).

%% @private recursive
build_h2(Dir,DirInfo,Acc) ->
    Items = filelib:wildcard("*",Dir),
    SubDirs = lists:filtermap(fun(RelPath) ->
                                    AbsPath = filename:join(Dir,RelPath),
                                    case filelib:is_dir(AbsPath) of
                                        false -> false;
                                        true ->
                                            {ok,FileInfo} = file:read_file_info(AbsPath),
                                            {true, {AbsPath, FileInfo}}
                                    end end, Items),
    Acc1 = ?Dict:put(Acc,{Dir,#h2{file_info = DirInfo,
                                   subdir_count = undefined}}), % length(SubDirs)
    lists:foldl(fun({SubDirF,FileInfoF},AccF) -> build_h2(SubDirF,FileInfoF,AccF) end, Acc1, SubDirs).

%% --------------------------------------------------
%% H3: sync relatives
%% --------------------------------------------------
build_h3(Dir) ->
    FromH1 = build_h1(Dir),
    build_h3(Dir, FromH1).

build_h3(Dir,FromH1) ->
    Prefix = string:trim(Dir, trailing, "/") ++ "/",
    Len = length(Prefix),
    FunRel = fun(Path) -> string:right(Path,erlang:max(0,length(Path) - Len)) end,
    F = fun({AbsPath,#h1{file_info=#file_info{type=Type,size=Size,mtime=MTime}}},Acc) ->
                case Type of
                    'regular' -> Acc#{FunRel(AbsPath) => {Type,MTime,Size}};
                    'directory' -> Acc#{FunRel(AbsPath) => {Type,{{1970,1,1},{0,0,0}},0}}; % DIR_MTIME: directory doesn't need mtime check, And size check can cause problems too
                    _ -> Acc
                end end,
    ?Dict:fold(F, #{}, FromH1).

%% --------------------------------------------------
%% H4: hash on syncing relatives
%% --------------------------------------------------
build_h4(Dir) ->
    FromH3 = build_h3(Dir),
    build_h4(Dir, FromH3).

build_h4(_Dir,FromH3) ->
    erlang:phash2(?Dict:fold(fun({K,V},Acc) -> Acc + erlang:phash2({K,V}) end, 0, FromH3)).

build_checksum(Dir,FromH1) ->
    build_h4(Dir,build_h3(Dir,FromH1)).

%% --------------------------------------------------
%% Refresh H2 from previous value
%% Economy 90% on heavy operation filelib:wildcard
%% --------------------------------------------------
refresh_h2(_Dir,H2) ->
    % step 1. partition hash by paths (non-modified, deledted, modified)
    F1 = fun({AbsPath,#h2{file_info=#file_info{type=Type,inode=Inode,mtime=MTime}}=Info0}, {NonMod,Del,Mod}) ->
                case file:read_file_info(AbsPath) of
                    {error,enoent} -> {NonMod, ?Dict:put(Del,{AbsPath,Info0}), Mod};
                    {ok,#file_info{type=Type1}=Info1} when Type/=Type1 -> {NonMod, ?Dict:put(Del,{AbsPath,Info1}), Mod};
                    {ok,#file_info{type=Type,inode=Inode,mtime=MTime}=DirInfo1} -> {?Dict:put(NonMod,{AbsPath,Info0#h2{file_info=DirInfo1}}), Del, Mod};
                    {ok,#file_info{}=DirInfo1} -> {NonMod, Del, ?Dict:put(Mod,{AbsPath,Info0#h2{file_info=DirInfo1}})}
                end end,
    {NonMod,Del,Mod} = ?Dict:fold(F1, {empty_hash(),empty_hash(),empty_hash()}, H2),
    % step 2. check modified items, update them, and explore added items (by path) with whole sub tree
    F2 = fun({SubDir,#h2{}=Info0},{AccAdd,AccMod}) ->
                Items = filelib:wildcard("*",SubDir),
                SubDirsPaths = lists:filtermap(fun(RelPath) ->
                                                    AbsPath = filename:join(SubDir,RelPath),
                                                    case filelib:is_dir(AbsPath) of
                                                        false -> false;
                                                        true -> {true, AbsPath}
                                                    end end, Items),
                AccMod1 = ?Dict:put(AccMod,{SubDir,Info0#h2{subdir_count = undefined}}), % length(SubDirsPaths)
                lists:foldl(fun(SubDirF,{AccAddF,AccModF}) ->
                                    case ?Dict:find(NonMod,SubDirF,undefined) of
                                        undefined ->
                                            case ?Dict:find(Mod,SubDirF,undefined) of
                                                undefined ->
                                                    AccAddF1 = ?Dict:fold(fun({Key,Val}, AccX) -> ?Dict:put(AccX,{Key,Val}) end, AccAddF, build_h2(SubDirF)),
                                                    {AccAddF1, AccModF};
                                                _ -> {AccAddF,AccModF} % item was or would be explored itself in self iteration of Mod
                                            end;
                                        _ -> {AccAddF,AccModF} % item not changed
                                    end end, {AccAdd,AccMod1}, SubDirsPaths)
        end,
    {Add,Mod1} = ?Dict:fold(F2, {empty_hash(),Mod}, Mod),
    % step 3. build NewH2
    Merged = ?Dict:merge([empty_hash(), NonMod, Add, Mod1]),
    lists:foreach(fun(X) -> ?Dict:close(X) end, [Del,Add,Mod1,NonMod]),
    Merged.

%% --------------------------------------------------
%% Refresh H1 from previous value
%% Economy 90% on heavy operation filelib:wildcard
%% --------------------------------------------------
refresh_h1(Dir,H1) ->
    % step 1. partition hash by paths (non-modified, deledted, modified)
    F1 = fun({AbsPath,#h1{file_info=#file_info{type=Type,inode=Inode,mtime=MTime,size=Size}}=Info0}, {NonMod,Del,Mod}) ->
                case file:read_file_info(AbsPath) of
                    {error,enoent} -> {NonMod, ?Dict:put(Del,{AbsPath,Info0}), Mod};
                    {ok,#file_info{type=Type,inode=Inode,mtime=MTime,size=Size}=ItemInfo1} -> {?Dict:put(NonMod,{AbsPath,Info0#h1{file_info=ItemInfo1}}), Del, Mod};
                    {ok,#file_info{type=Type1}=ItemInfo1} when Type1=='directory';Type1=='regular' -> {NonMod, Del, ?Dict:put(Mod,{AbsPath,Info0#h1{file_info=ItemInfo1}})};
                    {ok,#file_info{}} -> {NonMod, ?Dict:put(Del,{AbsPath,Info0}), Mod}
                end end,
    {NonMod,Del,Mod} = ?Dict:fold(F1, {empty_hash(),empty_hash(),empty_hash()}, H1),
    % step 2. check modified items, update them, and explore added items (by path) with whole sub tree
    F2 = fun({SubFile,#h1{file_info=#file_info{type='regular'}}=Info0},{AccAdd,AccMod}) ->
                {AccAdd,?Dict:put(AccMod,{SubFile,Info0#h1{md5=md5(SubFile)}})};
            ({SubDir,#h1{file_info=#file_info{type='directory'}}=Info0},{AccAdd,AccMod}) ->
                Items = filelib:wildcard("*",SubDir),
                Infos = lists:filtermap(fun(RelPath) ->
                                                AbsPath = filename:join(Dir,RelPath),
                                                case file:read_file_info(AbsPath) of
                                                    {ok,FileInfo} ->
                                                        {true, {AbsPath, FileInfo}};
                                                    {error,enoent} ->
                                                        % symlink failure
                                                        false;
                                                    {error,eloop} ->
                                                        % symlink/mount loop failure
                                                        false;
                                                    {error,_} ->
                                                        % other failures
                                                        false
                                                end end, Items),
                % first files
                SubFiles = lists:filter(fun({_,#file_info{type='regular'}}) -> true; (_) -> false end, Infos),
                {AccAdd1,AccMod1} = lists:foldl(fun({SubFileF,FileInfoF}, {AccAddF,AccModF}) ->
                                                        case ?Dict:find(NonMod,SubFileF,undefined) of
                                                            undefined ->
                                                                case ?Dict:find(AccModF,SubFileF,undefined) of
                                                                    undefined -> {?Dict:put(AccAddF, {SubFileF,#h1{file_info=FileInfoF,md5=md5(SubFileF)}}), AccMod};
                                                                    InfoF -> {AccAddF, ?Dict:put(AccModF, {SubFileF,InfoF#h1{file_info=FileInfoF,md5=md5(SubFileF)}})} % file is already in modified list, update md5
                                                                end;
                                                            _ -> {AccAddF,AccModF} % file not changed
                                                        end end, {AccAdd,AccMod}, SubFiles),
                % then sub directories
                SubDirsPaths = lists:filtermap(fun({Path,#file_info{type='directory'}}) -> {true,Path}; (_) -> false end, Infos),
                AccMod2 = ?Dict:put(AccMod1,{SubDir,Info0#h1{subdir_count = undefined}}), % length(SubDirsPaths)
                lists:foldl(fun(SubDirF,{AccAddF,AccModF}) ->
                                    case ?Dict:find(NonMod,SubDirF,undefined) of
                                        undefined ->
                                            case ?Dict:find(Mod,SubDirF,undefined) of
                                                undefined ->
                                                    AccAddF1 = ?Dict:fold(fun({Key,Val}, AccX) -> ?Dict:put(AccX,{Key,Val}) end, AccAddF, build_h1(SubDirF)),
                                                    {AccAddF1, AccModF};
                                                _ -> {AccAddF,AccModF} % item is already in modified list and would be explored itself in self iteration of Mod
                                            end;
                                        _ -> {AccAddF,AccModF} % item not changed
                                    end end, {AccAdd1,AccMod2}, SubDirsPaths);
             (_,Acc) -> Acc
         end,
    {Add,Mod1} = ?Dict:fold(F2, {empty_hash(),Mod}, Mod),
    % step 3. build NewH2
    Merged = ?Dict:merge([empty_hash(), NonMod, Add, Mod1]),
    lists:foreach(fun(X) -> ?Dict:close(X) end, [Del,Add,Mod1,NonMod]),
    Merged.

%% ---------------------------------------------
get_info(#h1{file_info=FileInfo}) -> FileInfo;
get_info(#h2{file_info=FileInfo}) -> FileInfo;
get_info(#file_info{}=FileInfo) -> FileInfo.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----
%% 900% of read_file_info
md5(Path) when is_list(Path) -> undefined;
md5(Path) when is_list(Path) ->
    {ok,MD5} = ?BU:file_md5(Path),
    ?BU:bin_to_hexstr(MD5).

%% ----
empty_hash() -> ets:new(cache,[public,set]).
%empty_hash() -> #{}.
