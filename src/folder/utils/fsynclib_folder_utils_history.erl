%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.07.2021
%%% @doc History routines
%%%     Load history from disk
%%%     Build history from disk
%%%     Refresh history from disk
%%%     Update history by disk operations

-module(fsynclib_folder_utils_history).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    load/2,
    refresh/3
]).

-export([push/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Load history dets from disk or init new
%% ------------------------------------------
load(Code,Dir) ->
    HistoryPath = get_history_path(Code),
    ?BLfilelib:ensure_dir(HistoryPath),
    case dets:is_dets_file(HistoryPath) of
        true ->
            case dets:open_file(HistoryPath) of
                {ok,Ref} -> Ref;
                {error,_}=Err ->
                    ?LOG('$error',"~ts. '~ts' history dets load failed: ~120tp",[?APP,Code,Err]),
                    ok = ?BLfile:delete(HistoryPath),
                    load(Code,Dir)
            end;
        _ ->
            case ?BLfile:read_file_info(HistoryPath) of
                {ok,#file_info{type='regular'}} ->
                    ok = ?BLfile:delete(HistoryPath);
                {ok,#file_info{type='directory'}} ->
                    ?BU:directory_delete(HistoryPath);
                _ -> ok
            end,
            Opts = [{file,HistoryPath},
                    {auto_save,60000},
                    {type,set}],
            case dets:open_file(make_ref(),Opts) of
                {ok,Ref} -> Ref;
                {error,_}=Err ->
                    ?LOG('$error',"~ts. '~ts' history dets init failed: ~120tp",[?APP,Code,Err]),
                    throw(Err)
            end
    end.

%% ------------------------------------------
%% Refresh history dets by actual cache
%% ------------------------------------------
refresh(Code,History,CacheH1) ->
    case dets:info(History,size) of
        0 -> build(Code,History,CacheH1);
        _ ->
            FunNextDt = fun(DT) -> calendar:gregorian_seconds_to_datetime(calendar:datetime_to_gregorian_seconds(DT) + 1) end,
            FunOp = fun('regular') -> modified; ('directory') -> created end,
            % deleted
            Map = dets:foldl(fun({K,[]},Acc) ->
                                        dets:delete(History,K),
                                        Acc;
                                ({K,[{deleted,_Type,_MTime,_Opts}|_]=V},Acc) ->
                                        case ?Dict:find(CacheH1,K,undefined) of
                                            undefined -> Acc;
                                            Info ->
                                                #file_info{type=Type1,mtime=MTime1,size=Size1} = ?CacheBuilder:get_info(Info),
                                                dets:insert(History,{K,[{FunOp(Type1),Type1,MTime1,[Size1]} | V]}), % add created/modified
                                                Acc#{K => ok}
                                        end;
                                ({K,[{_Op,Type,MTime,Opts}|_]=V},Acc) ->
                                        case ?Dict:find(CacheH1,K,undefined) of
                                            undefined ->
                                                dets:insert(History,{K,[{deleted,Type,FunNextDt(MTime),[]} | V]}), % add deleted
                                                Acc;
                                            Info ->
                                                case ?CacheBuilder:get_info(Info) of
                                                    #file_info{type=Type,mtime=MTime,size=Size} when Opts==[Size] ->
                                                        Acc;
                                                    #file_info{type=Type,mtime=MTime1,size=Size1} ->
                                                        dets:insert(History,{K,[{modified,Type,MTime1,[Size1]} | V]}), % add modified
                                                        Acc;
                                                    #file_info{type=Type1,mtime=MTime1,size=Size1} ->
                                                        dets:insert(History,{K,[{FunOp(Type1),Type1,MTime1,[Size1]},{deleted,Type,FunNextDt(MTime),[]} | V]}), % add deleted, then created/modified
                                                        Acc
                                                end end end, #{}, History),
            % created
            ets:foldl(fun({K,Info},_) ->
                            case maps:get(K,Map,undefined) of
                                undefined ->
                                    #file_info{type=Type1,mtime=MTime1,size=Size1} = ?CacheBuilder:get_info(Info),
                                    dets:insert(History,{K,[{FunOp(Type1),Type1,MTime1,[Size1]}]}); % add created/modified
                                _ -> ok
                            end end, ok, CacheH1),
            % shrink
            ExpireTS = ?BU:timestamp() - ?CFG:history_deleted_keep_days() * 86400 * 1000,
            dets:foldl(fun({_,[_]},_) -> ok;
                          ({K,[{deleted,_,MTime,_}|_]},_) when MTime < ExpireTS -> dets:delete(History,K);
                          ({K,[Last|_]},_) -> dets:insert(History,{K,[Last]})
                       end, ok, History),
            History
    end.

%% @private
%% Fill history by actual cache
build(Code,History,CacheH1) ->
    ?LOG('$info',"~ts. '~ts' history dets build by cache",[?APP,Code]),
    dets:delete_all_objects(History),
    Ets = ets:new(temp,[set]),
    Items = ?Dict:fold(fun({K,V},Acc) ->
                            #file_info{type=Type,mtime=MTime,size=Size} = ?CacheBuilder:get_info(V),
                            [{K,Type,MTime,Size} | Acc]
                       end, [], CacheH1),
    Items1 = lists:sort(fun({Path1,_,MTime1,_},{Path2,_,MTime2,_}) -> (MTime1<MTime2) orelse (MTime1==MTime2 andalso Path1 =< Path2) end, Items),
    ok = lists:foreach(fun({Path,'directory'=Type,MTime,Size}) -> ets:insert(Ets,{Path,[{created,Type,MTime,[Size]}]});
                          ({Path,'regular'=Type,MTime,Size}) -> ets:insert(Ets,{Path,[{modified,Type,MTime,[Size]}]})
                       end, Items1),
    ok = dets:from_ets(History,Ets),
    ets:delete(Ets),
    ?LOG('$info',"~ts. '~ts' history dets build by cache ok: ~120tp items",[?APP,Code,dets:info(History,size)]),
    History.

%% ------------------------------------
%% ------------------------------------
push(AbsPath, {Operation,Type,TS,Size}, State) when is_integer(TS) ->
    push(AbsPath, {Operation,Type,utc_dt(TS),Size}, State);
push(AbsPath, Item, #fstate{history=History}) ->
    push(AbsPath, Item, History);
push(AbsPath, {_Operation,_Type,_DT,_Size}=Item, History) when is_reference(History) ->
    case dets:lookup(History,AbsPath) of
        [] -> dets:insert(History,{AbsPath,[Item]});
        [{_,List}] -> dets:insert(History,{AbsPath,[Item|List]})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
get_history_path(Code) ->
    {ok,CWD} = file:get_cwd(),
    filename:join([CWD,"fsync_history",?BU:to_list(Code),"history.dets"]).

%% @private
utc_dt(TS) ->
    {D,T,_} = ?BU:ticktoutc(TS),
    {D,T}.