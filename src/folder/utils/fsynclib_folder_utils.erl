%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.07.2021
%%% @doc

-module(fsynclib_folder_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    rel_path/2,
    abs_path/2
]).

-export([find_children/2]).

-export([update_checksum/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------
rel_path(RootDir,RootDir) -> "";
rel_path(RootDir,AbsPath) ->
    % RootDir ++ "/"
    lists:nthtail(length(RootDir) + 1, AbsPath).

%% ------------------------------
abs_path(RootDir,RelPath) ->
    filename:join(RootDir,RelPath).

%% ------------------------------
find_children(Dir,#fstate{cache_h1=H1}) ->
    find_children(Dir,H1);
find_children(Dir,H1) ->
    DirLen = length(Dir),
    ?Dict:fold(fun({K,V},Acc) ->
                    case length(K) of
                        L when L =< DirLen -> Acc;
                        _ ->
                            case string:left(K,DirLen) of
                                Dir -> [{K,V}|Acc];
                                _ -> Acc
                            end end end, [], H1).

%% ------------------------------
update_checksum(At,#fstate{code=Code,path=RootDir,cache_h1=H1}=State) ->
    {Mks,HC} = timer:tc(fun() -> ?CacheBuilder:build_checksum(RootDir,H1) end),
    ?LOG('$trace',"~ts. '~ts'. update checksum (~ts): ~120tp (~p mks, ~p items)",[?APP,Code,At,HC,Mks,?Dict:size(H1)]),
    State#fstate{hc = HC}.

%% ====================================================================
%% Internal functions
%% ====================================================================