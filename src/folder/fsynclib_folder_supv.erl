%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.07.2021
%%% @doc

-module(fsynclib_folder_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1,get_linkname/1, get_regname/1]).
-export([start_child/2, restart_child/2, terminate_child/2, delete_child/2, get_childspec/2]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% starts supv
start_link(Opts) ->
    Code = ?BU:get_by_key('code', Opts),
    RegName = get_linkname(Code),
    supervisor:start_link({local,RegName}, ?MODULE, Opts).

%% returns name of supv for linking to top supv
get_linkname(Code) when is_binary(Code); is_atom(Code) ->
    get_regname(Code).

%% returns reg name of supv
get_regname(Code) when is_binary(Code); is_atom(Code) ->
    ?BU:to_atom_new(?BU:strbin("~ts:supv;code:~ts",[?APP,Code])).

%% ====================================================================
%% API functions (ext management)
%% ====================================================================

start_child(Code, ChildSpec) ->
    supervisor:start_child(get_regname(Code), ChildSpec).

restart_child(Code, Id) ->
    supervisor:restart_child(get_regname(Code), Id).

terminate_child(Code, Id) ->
    supervisor:terminate_child(get_regname(Code), Id).

delete_child(Code, Id) ->
    supervisor:delete_child(get_regname(Code), Id).

get_childspec(Code, Id) ->
    supervisor:get_childspec(get_regname(Code), Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    Code = ?BU:get_by_key('code', Opts),
    FacadeRegName = ?BU:to_atom(?BU:get_by_key('regname', Opts)), % crash if not
    %------------
    FacadeSrv = {?FSRV, {?FSRV, start_link, [FacadeRegName, Opts]}, permanent, 1000, worker, [?FSRV]},
    % -----------
    ?LOG('$info', "~ts. '~ts' supv inited as service", [?APP, Code]),
    {ok, {{one_for_all, 10, 2}, [FacadeSrv]}}.