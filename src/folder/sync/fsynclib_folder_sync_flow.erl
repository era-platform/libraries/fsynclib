%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.07.2021
%%% @doc Sync process
%%%     request cache h3 from nodes to sync
%%%     build difference list of item' names
%%%     request history from nodes for selected items
%%%     build list to download to local from remote (if not deleted in own history)
%%%     build list to delete in local (if deleted in remote history)
%%%     download,
%%%     delete

-module(fsynclib_folder_sync_flow).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_sync/2]).

-export([sync_loop/3,
         do_sync/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
start_sync([],#fstate{}=State) -> State;
start_sync(Nodes,#fstate{code=Code,self=Self,path=RootDir,cache_h1=H1,history=History,sync_pid=Pid,sync_ref=Ref}=State) ->
    case Pid of
        undefined ->
            Ref1 = make_ref(),
            FunResult = fun(Reply) -> Self ! {fsync_reply,Ref1,Reply} end,
            Pid1 = spawn_link(fun() -> sync_loop(Ref1,Code,RootDir) end),
            ?LOG('$trace',"~ts. '~ts'. fsync pid: ~120tp",[?APP,Code,Pid1]),
            Pid1 ! {sync, Ref1, {Nodes,H1,History,FunResult}},
            State#fstate{sync_pid = Pid1,
                         sync_ref = Ref1};
        _ when is_pid(Pid) ->
            case process_info(Pid,status) of
                undefined ->
                    start_sync(Nodes,State#fstate{sync_pid=undefined,sync_ref=undefined});
                _ ->
                    FunResult = fun(Reply) -> Self ! {fsync_reply,Ref,Reply} end,
                    Pid ! {sync, Ref, {Nodes,H1,History,FunResult}},
                    State
            end
    end.

%% ====================================================================
%% Spawned process
%% ====================================================================

%% loop
sync_loop(Ref,Code,Dir) ->
    receive
        {sync, Ref, {Nodes,H1,History,FunResult}} ->
            do_sync_catch(Nodes,Code,Dir,H1,History,FunResult),
            ?MODULE:sync_loop(Ref,Code,Dir);
        {stop, Ref} -> ok
    end.

%% -------------------
%% @private
do_sync_catch(Nodes,Code,Dir,H1,History,FunResult) ->
    try
        Reply = ?MODULE:do_sync(Nodes,Code,Dir,H1,History),
        FunResult(Reply)
    catch E:R:ST ->
        ?LOG('$crash', "fsync crashed: {~120tp, ~120tp}~n\tStack: ~120tp", [E,R,ST])
    end.

%% @private
do_sync(Nodes,Code,LDir,LH1,LHistory) ->
    ?LOG('$trace',"~ts. '~ts'. fsync: ~120tp",[?APP,Code,Nodes]),
    X = lists:foldl(fun(Node,Acc) ->
                            case ?BLrpc:call(Node,?FSRV,get_cache_data,[Code,[node()]],5000) of
                                {ok,RDir,RH3} ->
                                    Local = lists:sort(local(Node,{LDir,LH1},{RDir,RH3})),
                                    Remote = lists:sort(remote(Node,{LDir,LH1},{RDir,RH3})),
                                    [{Node,RDir,RH3,{Local,Remote}} | Acc];
                                _ -> Acc
                            end end, [], Nodes),
    X1 = lists:sort(fun({_,_,_,{L1,R1}},{_,_,_,{L2,R2}}) -> length(L1) + length(R1) =< length(L2) + length(R2) end, X),
    ?LOG('$trace',"fsync nodes sequence: ~120tp",[lists:map(fun({Node,_,_,{L,R}}) -> {Node,length(L),length(R)} end, X1)]),
    ChCount = lists:foldl(fun({Node,RDir,RH3,{L,R}},Acc) ->
                                ?LOG('$debug',"fsync node '~ts' files: ~n\tLocals: ~120tp~n\tRemote: ~120tp",[Node,L,R]),
                                case ?BLrpc:call(Node,?FSRV,get_history_for,[Code,[node(),lists:usort(R++L)]],5000) of
                                    {ok,RHistMap} ->
                                        NewCnt = download_files(Node,LDir,RDir,LH1,LHistory,RH3,RHistMap,R), % download files
                                        DelCnt = remove_files(Node,LDir,LH1,LHistory,RHistMap,L), % remove files if they are found in remote history as deleted
                                        Acc + DelCnt + NewCnt;
                                    _ -> Acc
                                end end, 0, X1),
    ?LOG('$trace',"~ts. '~ts'. fsync done (~p changes)",[?APP,Code,ChCount]),
    {ok,ChCount}.

%% -------------------
%% @private
%% build list of relative paths, that exist on local node and not exists or less datetime on remote node
local(_SyncNode,{LDir,LH1},{_RDir,RH3}) ->
    ?Dict:fold(fun({K,V},Acc) ->
                        RelPath = ?FldUtils:rel_path(LDir,K),
                        case maps:get(RelPath,RH3,undefined) of
                            undefined -> [RelPath|Acc];
                            {Type,MTime,Size} ->
                                case ?CacheBuilder:get_info(V) of
                                    #file_info{type=Type,size=Size,mtime=MTime} -> Acc;
                                    #file_info{type='directory'} -> Acc; % DIR_MTIME: directory doesn't need mtime check
                                    %#file_info{mtime=LMTime} when LMTime > MTime -> [RelPath|Acc];
                                    %#file_info{mtime=MTime} -> [RelPath|Acc];
                                    %#file_info{} -> Acc;
                                    #file_info{} -> [RelPath|Acc] % if moved file, file older, moved later. So any change - we should use history to define direction
                                end end end, [], LH1).

%% @private
%% build list of relative paths, that exist on remote node and not exists or less datetime on current node
remote(_SyncNode,{LDir,LH1},{_RDir,RH3}) ->
    maps:fold(fun(RelPath,{Type,MTime,Size},Acc) ->
                        AbsPath = ?FldUtils:abs_path(LDir,RelPath),
                        case ?Dict:find(LH1,AbsPath,undefined) of
                            undefined -> [RelPath|Acc];
                            Info ->
                                case ?CacheBuilder:get_info(Info) of
                                    #file_info{type=Type,size=Size,mtime=MTime} -> Acc;
                                    #file_info{type='directory'} -> Acc; % DIR_MTIME: directory doesn't need mtime check
                                    %#file_info{mtime=LMTime} when LMTime < MTime -> [RelPath|Acc];
                                    %#file_info{mtime=MTime} when CurNode > Node -> [RelPath|Acc];
                                    %#file_info{} -> Acc
                                    #file_info{} -> [RelPath|Acc] % if moved file, file older, moved later. So any change - we should use history to define direction
                                end end end, [], RH3).

%% -------------------
%% @private
download_files(SyncNode,LDir,RDir,LH1,LHistory,RH3,RHistMap,R_RelPaths) ->
    CurNode = node(),
    lists:foldl(fun(RelPath,Acc) ->
                        LocalAbsPath = ?FldUtils:abs_path(LDir,RelPath),
                        RemoteAbsPath = ?FldUtils:abs_path(RDir,RelPath),
                        {RType,RMTime1,RSize} = maps:get(RelPath,RH3,undefined),
                        Skip = case ?Dict:find(LH1,LocalAbsPath,undefined) of
                                   undefined -> false;
                                   Info ->
                                       case ?CacheBuilder:get_info(Info) of
                                           #file_info{type=RType,mtime=RMTime1,size=RSize} -> true;
                                           _ -> false
                                       end end,
                        case maps:get(RelPath,RHistMap,undefined) of
                            undefined ->
                                ?LOG('$trace',"fsync '~ts' download skipped (sync error): '~ts'",[SyncNode,RelPath]),
                                Acc;
                            _ when Skip ->
                                ?LOG('$trace',"fsync '~ts' download skipped (already done): '~ts'",[SyncNode,RelPath]),
                                Acc;
                            {_,_,RMTime2,_} ->
                                case dets:lookup(LHistory,LocalAbsPath) of
                                    [{_,[{deleted,_,LMTime,_}|_]}] when LMTime >= RMTime2 ->
                                        ?LOG('$trace',"fsync '~ts' download skipped (deleted later): '~ts'",[SyncNode,RelPath]),
                                        Acc;
                                    [{_,[{_,_,LMTime,_}|_]}] when LMTime > RMTime2 ->
                                        ?LOG('$trace',"fsync '~ts' download skipped (modified later): '~ts'",[SyncNode,RelPath]),
                                        Acc;
                                    [{_,[{_,_,LMTime,_}|_]}] when LMTime == RMTime2, SyncNode > CurNode ->
                                        ?LOG('$trace',"fsync '~ts' download skipped (conflict, node priority): '~ts'",[SyncNode,RelPath]),
                                        Acc;
                                    _ ->
                                        download({SyncNode,RemoteAbsPath,LocalAbsPath},{LH1,LHistory},{RelPath,RType,RMTime1,RMTime2}),
                                        Acc + 1
                                end end end, 0, R_RelPaths).

%% @private
remove_files(SyncNode,LDir,LH1,LHistoryDets,RHistMap,L_RelPaths) ->
    lists:foldl(fun(RelPath,Acc) ->
                        case maps:get(RelPath,RHistMap,undefined) of
                            {'deleted',_RType,RMTime,_} ->
                                LocalAbsPath = ?FldUtils:abs_path(LDir,RelPath),
                                Skip = case ?Dict:find(LH1,LocalAbsPath,undefined) of
                                           undefined -> true;
                                           _ -> false
                                       end,
                                case dets:lookup(LHistoryDets,LocalAbsPath) of
                                    [{_,[{_,_,LMTime,_}|_]}] when LMTime =< RMTime, Skip ->
                                        ?LOG('$trace',"fsync '~ts' delete skipped (already done): '~ts'",[SyncNode,RelPath]),
                                        Acc;
                                    [{_,[{_,LType,LMTime,_}|_]}] when LMTime =< RMTime ->
                                        delete(SyncNode,RelPath,LocalAbsPath,LType,RMTime,LH1,LHistoryDets),
                                        Acc + 1;
                                    _ ->
                                        ?LOG('$trace',"fsync '~ts' delete skipped (modified later): '~ts'",[SyncNode,RelPath]),
                                        Acc
                                end;
                            _ ->
                                ?LOG('$trace',"fsync '~ts' delete skipped (non-deleted): '~ts'",[SyncNode,RelPath]),
                                Acc
                        end end, 0, L_RelPaths).

%% -------------------
%% @private
download({Node,RemoteAbsPath,LocalAbsPath},{H1,History},{RelPath,Type,_ItemDT,EventDT}) ->
    % to disk
    case Type of
        'regular' ->
            ?LOG('$trace',"fsync '~ts' download file: '~ts'...",[Node,RelPath]),
            ok = ?BLfilecopier:copy_file(Node, RemoteAbsPath, LocalAbsPath, fun(Level,Fmt,Args) -> ?LOG(Level,Fmt,Args) end, ?CFG:copier_bandwidth());
            %?BLfile:change_time(LocalAbsPath,ItemDT); % file after copying already has same attributes as original
        'directory' ->
            ?LOG('$trace',"fsync '~ts' ensure folder: '~ts'...",[Node,RelPath]),
            ok = ?BLfilelib:ensure_dir(filename:join(LocalAbsPath,"."))
            %?BLfile:change_time(LocalAbsPath,ItemDT) % DIR_MTIME: directory doesn't need mtime check
    end,
    {ok,#file_info{size=Size}} = ?BLfile:read_file_info(LocalAbsPath),
    % to cache
    ?Dict:put(H1,{LocalAbsPath,?CacheBuilder:build_info_h1(LocalAbsPath)}),
    % push to history
    ?History:push(LocalAbsPath,{modified,Type,EventDT,[Size]},History).

%% @private
delete(_,"",_,_,_,_,_) -> ok;
delete(_Node,_RelPath,AbsPath,Type,EventDT,H1,History) ->
    % remove from disk
    case Type of
        'regular' ->
            ?LOG('$trace',"fsync '~ts' delete file: '~ts'...",[_Node,_RelPath]),
            ?BLfile:delete(AbsPath);
        'directory' ->
            ?LOG('$trace',"fsync '~ts' delete folder: '~ts'...",[_Node,_RelPath]),
            ?BU:directory_delete(AbsPath)
    end,
    % remove from cache
    ?Dict:delete(H1,AbsPath),
    % TODO: refresh hc
    % push to history
    ?History:push(AbsPath,{deleted,Type,EventDT,[]},History).
