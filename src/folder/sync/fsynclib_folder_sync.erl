%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.07.2021
%%% @doc Sync routines
%%%     freezing handlers of connected nodes
%%%     checking sync state of connected nodes
%%%     build and return sync info
%%%     build and return cache h3
%%%     build and return last history record for requested items

-module(fsynclib_folder_sync).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    freeze_start/3,
    freeze_cancel/2
]).

-export([
    check_sync/2,
    on_sync_result/2,
    get_sync_info/2,
    get_cache_data/2,
    get_history_for/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions. Freeze/cancel/timeout
%% ====================================================================

%% --------------------------------------
%% Freezing handle events from filesystem
%% --------------------------------------
freeze_start(FromNode,Timeout,#fstate{code=_Code,state=S,freezed_by=FreezedBy}=State)
  when S=='$running'; S=='$freezed' ->
    Ref1 = make_ref(),
    ?LOG('$debug',"~ts. '~ts'. freezed by '~ts'",[?APP,_Code,FromNode]),
    FunTimeout = fun(StateF) -> on_freeze_timeout(FromNode,Ref1,StateF) end,
    TimerRef1 = erlang:send_after(Timeout,self(),{apply,FunTimeout}),
    State1 = State#fstate{state = '$freezed',
                          freezed_by = lists:keystore(FromNode,1,FreezedBy,{FromNode,Ref1,TimerRef1})},
    {ok, State1};
%%
freeze_start(_FromNode,_Timeout,#fstate{code=_Code,state=S}) ->
    ?LOG('$debug',"~ts. '~ts'. freeze from '~ts' skipped by state (~ts)",[?APP,_Code,_FromNode,S]),
    {error,{state_mismatch,S}}.

%% --------------------------------------
%% Stops freezing events, resume dequeue.
%% --------------------------------------
freeze_cancel(FromNode, #fstate{code=_Code,state='$freezed',freezed_by=FreezedBy}=State) ->
    case lists:keytake(FromNode,1,FreezedBy) of
        false ->
            ?LOG('$debug',"~ts. '~ts'. wrong freeze cancel from '~ts'.",[?APP,_Code,FromNode]),
            {error,{node_not_found,FromNode}};
        {value,{FromNode,_,TimerRef},[]} ->
            ?BU:cancel_timer(TimerRef),
            State1 = State#fstate{state = '$running',
                                  freezed_by = []},
            ?LOG('$debug',"~ts. '~ts'. freeze canceled from '~ts'. Running.",[?APP,_Code,FromNode]),
            State2 = ?Event:on_freeze_cancel(State1),
            {ok,State2};
        {value,{FromNode,_,TimerRef},Rest} ->
            ?BU:cancel_timer(TimerRef),
            ?LOG('$debug',"~ts. '~ts'. freeze canceled from '~ts'. Remain ~p locks.",[?APP,_Code,FromNode,length(Rest)]),
            {ok,State#fstate{freezed_by = Rest}}
    end;
%%
freeze_cancel(_FromNode, #fstate{code=_Code,state=S}) ->
    ?LOG('$debug',"~ts. '~ts'. wrong freeze cancel from '~ts' by state (~ts).",[?APP,_Code,_FromNode,S]),
    {error,{state_mismatch,S}}.

%% --------------------------------------
%% Auto resume dequeue on freeze timeout
%% --------------------------------------
on_freeze_timeout(FromNode,Ref,#fstate{code=_Code,state='$freezed',freezed_by=FreezedBy}=State) ->
    case lists:keytake(FromNode,1,FreezedBy) of
        {value,{FromNode,Ref,_},[]} ->
            State1 = State#fstate{state = '$running',
                                  freezed_by = []},
            ?LOG('$debug',"~ts. '~ts'. freeze timeout from '~ts'. Running",[?APP,_Code,FromNode]),
            ?Event:on_freeze_cancel(State1);
        {value,{FromNode,Ref,_},Rest} ->
            ?LOG('$debug',"~ts. '~ts'. freeze timeout from '~ts'. Remain ~p locks.",[?APP,_Code,FromNode,length(Rest)]),
            State#fstate{freezed_by = Rest};
        _ ->
            ?LOG('$debug',"~ts. '~ts'. wrong freeze timeout from '~ts'.",[?APP,_Code,FromNode]),
            State
    end;
%%
on_freeze_timeout(_FromNode,_,#fstate{code=_Code,state=_S}=State) ->
    ?LOG('$debug',"~ts. '~ts'. wrong freeze timeout from '~ts' by state (~ts).",[?APP,_Code,_FromNode,_S]),
    State.

%% ====================================================================
%% Public functions. Check_sync / result
%% ====================================================================

%% --------------------------------------
%% starts spawned linked pid to get sync info from nodes
%%     first ping, then freeze, then wait sync queue flushed, then request for sync_info, then cancel freeze
%%     all this time current node srv is also in freezed state
%% --------------------------------------
check_sync(#fstate{code=_Code,nodes=[]}=State,FunReply) when is_function(FunReply,1) ->
    ?LOG('$trace',"~ts. '~ts'. Sync skipped (no nodes).",[?APP,_Code]),
    FunReply(skip),
    State#fstate{sync_nodes = []};
check_sync(#fstate{code=Code,nodes=Nodes,hc=HC,freezed_by=FreezedBy}=State,FunReply) when is_function(FunReply,1) ->
    Pid = spawn_link(fun() ->
                            ZipNodes = lists:zip(lists:seq(1,length(Nodes)),Nodes),
                            ZipFuns = [{I,fun() -> ?BLping:ping(Node,500) end} || {I,Node} <- ZipNodes],
                            PingResult = ?BLmulticall:apply(ZipFuns,600),
                            Fnode = fun(I) -> ?BU:get_by_key(I,ZipNodes,undefined) end,
                            ?LOG('$trace',"~ts. '~ts'. Sync ping... ~120tp",[?APP,Code,Nodes]),
                            {PongNodes,PangNodes} =
                                lists:foldl(fun({{I,_},pong}, {X,Y}) -> {[Fnode(I)|X],Y};
                                               ({{I,_},_}, {X,Y}) -> {X,[Fnode(I)|Y]}
                                            end, {[],[]}, PingResult),
                            ?LOG('$trace',"~ts. '~ts'. Sync freeze... ~120tp",[?APP,Code,PongNodes]),
                            FreezeResult = ?BLmulticall:call_direct(PongNodes,{?FSRV,sync_freeze,[Code,node(),{freeze,11000}]},5000),
                            {FreezedNodes,FreezeStateNodes,FreezeTimeoutNodes} =
                                lists:foldl(fun({Node,ok},{X,Y,Z}) -> {[Node|X],Y,Z};
                                               ({Node,{error,{state,_}}},{X,Y,Z}) -> {X,[Node|Y],Z};
                                               ({Node,_},{X,Y,Z}) -> {X,Y,[Node|Z]}
                                            end, {[],[],[]}, FreezeResult),
                            ?LOG('$trace',"~ts. '~ts'. Sync flush... ~120tp",[?APP,Code,FreezedNodes]),
                            FlushResult = ?BLmulticall:call_direct(FreezedNodes,{?FSRV,wait_sync_queue_flushed,[Code,node(),5000]},5000),
                            {FlushedNodes,UnflushedNodes} =
                                lists:foldl(fun({Node,ok},{X,Y}) -> {[Node|X],Y};
                                               ({Node,_},{X,Y}) -> {X,[Node|Y]}
                                            end, {[],[]}, FlushResult),
                            ?LOG('$trace',"~ts. '~ts'. Sync get_sync_info... ~120tp",[?APP,Code,FlushedNodes]),
                            R1 = ?BLmulticall:call_direct(FlushedNodes,{?FSRV,get_sync_info,[Code,[node(),HC]]},5000),
                            R2 = lists:map(fun(Node) -> {Node,pang} end,PangNodes),
                            R3 = lists:map(fun(Node) -> {Node,freeze_state} end,FreezeStateNodes),
                            R4 = lists:map(fun(Node) -> {Node,freeze_timeout} end,FreezeTimeoutNodes),
                            R5 = lists:map(fun(Node) -> {Node,flush_timeout} end,UnflushedNodes),
                            ?LOG('$trace',"~ts. '~ts'. Sync cancel freeze... ~120tp",[?APP,Code,PongNodes]),
                            ?BLmulticall:call_direct(PongNodes,{?FSRV,sync_freeze,[Code,node(),cancel]},1000),
                            FunReply(R1 ++ R2 ++ R3 ++ R4 ++ R5)
                    end),
    ?LOG('$trace',"~ts. '~ts'. Sync started ~p...",[?APP,Code,Pid]),
    State#fstate{state = '$freezed',
                 freezed_by = [{self,undefined,undefined} | FreezedBy]}.

%% --------------------------------------
%% applies result of checking sync infos
%%    combine all nodes by results in groups
%% --------------------------------------
on_sync_result(skip,State) -> State;
on_sync_result(Result,#fstate{code=_Code,hc=HC,freezed_by=FreezedBy}=State) ->
    ResMap = lists:foldl(fun({Node,Res},Acc) when Res==pang; Res==freeze_state; Res==freeze_timeout; Res==flush_timeout; Res==undefined ->
                                Acc#{Res => [Node | maps:get(Res,Acc,[])]};
                            ({Node,{ok,#{hc:=NHC}}},Acc) when NHC==HC -> Acc#{sync => [Node | maps:get(sync,Acc,[])]};
                            ({Node,{ok,#{state:=S}}},Acc) when S=='$freezed' -> Acc#{ready_to_sync => [Node | maps:get(ready_to_sync,Acc,[])]};
                            ({Node,{ok,#{state:=S}}},Acc) when S/='$freezed' -> Acc#{not_freezed => [Node | maps:get(not_freezed,Acc,[])]};
                            ({Node,_}=R,Acc) ->
                                ?LOG('$debug',"~ts. '~ts'. Got unknown sync result: ~500tp",[?APP,_Code,R]),
                                Acc#{other => [Node | maps:get(other,Acc,[])]}
                         end, #{}, Result),
    ?LOG('$trace',"~ts. '~ts'. Got sync result: ~n\t~120tp",[?APP,_Code,ResMap]),
    SyncNodes = maps:get(sync,ResMap,[]),
    NodesToSync = maps:get(ready_to_sync,ResMap,[]), % also flushed
    State2 = case lists:keydelete(self,1,FreezedBy) of
                 [] ->
                     State1 = State#fstate{state = '$running',
                                           freezed_by = [],
                                           sync_nodes = SyncNodes},
                     ?Event:on_freeze_cancel(State1);
                 List ->
                     State#fstate{freezed_by=List,
                                  sync_nodes = SyncNodes}
             end,
    ShrinkedNodesToSync = shrink_nodes_to_sync(NodesToSync,Result),
    ?SyncFlow:start_sync(ShrinkedNodesToSync,State2).

%% @private
%% Shrink nodes by same hc.
%% Only one node with unique hc could be selected to synchronize from
shrink_nodes_to_sync(Nodes,Result) ->
    HCs = lists:foldl(fun(Node,Acc) ->
                            case ?BU:get_by_key(Node,Result,undefined) of
                                {ok,#{hc:=NHC}} -> Acc#{NHC => [Node|maps:get(NHC,Acc,[])]};
                                _ -> Acc
                            end end, #{}, Nodes),
    maps:fold(fun(_HC,HcNodes,Acc) -> [HcNode|_]=HcNodes, [HcNode|Acc] end, [], HCs).

%% --------------------------------------
%% Return sync info
%% --------------------------------------
get_sync_info([FromNode,RHC],#fstate{code=_Code,state=S,cache_h1=CacheH1,hc=HC0,nodes=Nodes,sync_nodes=SyncNodes}=State)
  when S=='$freezed'; S=='$running' ->
    #fstate{hc=HC2}=State1 = case HC0 of
                                 undefined -> ?FldUtils:update_checksum(b,State);
                                 _ -> State
                             end,
    Info = #{state => S,
             size => ?Dict:size(CacheH1),
             sync_nodes => SyncNodes,
             hc => HC2},
    ?LOG('$debug',"~ts. '~ts'. Prepared sync_info for '~ts' (hc=~p): ~n\t~120tp",[?APP,_Code,FromNode,RHC,Info]),
    State2 = case {lists:member(FromNode,Nodes), lists:member(FromNode,SyncNodes)} of
                 {true,false} when HC2==RHC ->
                     % external sync found same HC when is not in sync_nodes yet
                     State1#fstate{sync_nodes = lists:sort([FromNode|SyncNodes])};
                 {true,false} ->
                     % external sync found another HC when is not in sync_nodes yet
                     self() ! {force_sync_node,FromNode},
                     State1;
                 {true,true} when HC2/=RHC ->
                     self() ! {force_sync_node,FromNode},
                     State1;
                 _ ->
                     State1
             end,
    {{ok,Info}, State2};
%%
get_sync_info([_FromNode],#fstate{code=_Code,state=S}=State) ->
    Info = #{state => S},
    ?LOG('$debug',"~ts. '~ts'. Prepared sync_info for ~ts: ~n\t~120tp",[?APP,_Code,_FromNode,Info]),
    {{ok,Info}, State}.

%% --------------------------------------
%% Return current cache H3::map(RelPath => {Type,Size,MTime})
%% --------------------------------------
get_cache_data([_FromNode],#fstate{code=_Code,path=RootDir,cache_h1=CacheH1}=State) ->
    H3 = ?CacheBuilder:build_h3(RootDir,CacheH1),
    ?LOG('$debug',"~ts. '~ts'. Prepared cache_data for ~ts: ~n\t~120tp",[?APP,_Code,_FromNode,H3]),
    {{ok,RootDir,H3}, State}.

%% --------------------------------------
%% Return last history info for selected Relative paths :: map(RelPath => {OpType,Type,MTime,[Size]})
%% --------------------------------------
get_history_for([_FromNode,RelPaths],#fstate{code=_Code,path=RootDir,history=History}=State) ->
    Map = lists:foldl(fun(RelPath, Acc) ->
                            AbsPath = ?FldUtils:abs_path(RootDir,RelPath),
                            case dets:lookup(History,AbsPath) of
                                [{_,[Operation|_]}] -> Acc#{RelPath => Operation};
                                _ ->
                                    ?LOG('$trace',"~ts. '~ts'. History for ~ts not found",[?APP,_Code,AbsPath]),
                                    Acc
                            end end, #{}, RelPaths),
    ?LOG('$debug',"~ts. '~ts'. Prepared history for ~ts: ~n\t~120tp",[?APP,_Code,_FromNode,Map]),
    {{ok,Map}, State}.
