%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.05.2021
%%% @doc Gen_server of starting and listening of os:pid by cmdline. Send data to handler_pid
%%%    Mservice :: map()
%%%      code :: binary() | atom(),
%%%      name :: binary(),
%%%      cmdline :: binary(),
%%%      workdir :: binary(),
%%%      restart_mode :: permanent | transient | temporary
%%%      pidfile_path :: binary(),
%%%      handler_pid :: pid(),
%%%      x_regname :: atom()

-module(fsynclib_folder_port_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start/1,
         stop/1,
         check_alive/1]).

-export([start_link/2]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(pstate, {
    mservice :: map(),
    handler_pid :: pid(),
    port :: undefined | port(),
    os_pid :: undefined | non_neg_integer(),
    regname :: atom(),
    code :: binary() | atom(),
    name :: binary(),
    ref :: reference(),
    last_exit_status :: undefined | integer(),
    last_exit_ts = [] :: [non_neg_integer()]
}).

%% ====================================================================
%% API functions
%% ====================================================================

%%
start(Mservice) ->
    Code = maps:get('code',Mservice),
    RegName = get_regname(Mservice),
    ChildSpec = {RegName, {?MODULE, start_link, [RegName,Mservice]}, permanent, 1000, worker, [?MODULE]},
    ?FSUPV:start_child(Code,ChildSpec).

%%
stop(Mservice) ->
    RegName = get_regname(Mservice),
    case whereis(RegName) of
        undefined -> {error,noproc};
        Pid when is_pid(Pid) ->
            gen_server:call(Pid, 'stop'),
            {ok,Pid}
    end.

%%
check_alive(Mservice) ->
    RegName = get_regname(Mservice),
    case whereis(RegName) of
        undefined -> false;
        Pid when is_pid(Pid) -> true
    end.

%%
start_link(RegName,Mservice) ->
    gen_server:start_link({local, RegName}, ?MODULE, Mservice#{'x_regname' => RegName}, []).

%%
get_regname(Mservice) ->
    Code = maps:get('code',Mservice),
    Name = maps:get('name',Mservice),
    ?BU:to_atom_new(?BU:strbin("fsync_port#code:~ts;n:~ts",[Code,Name])).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Mservice) ->
    Ref = make_ref(),
    Code = maps:get('code',Mservice),
    Name = maps:get('name',Mservice),
    HPid = maps:get('handler_pid',Mservice),
    State1 = #pstate{regname = maps:get('x_regname',Mservice),
                     handler_pid = HPid,
                     mservice = Mservice,
                     code = Code,
                     name = Name,
                     ref = Ref},
    case process_info(HPid,status) of
        undefined ->
            ?LOG('$info', "~ts. '~ts' '~ts' port srv auto stop", [?APP,Code,Name]),
            gen_server:cast(self(), 'stop');
        _ ->
            ?LOG('$info', "~ts. '~ts' '~ts' port srv inited", [?APP,Code,Name]),
            gen_server:cast(self(), {'init',Ref})
    end,
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call('stop', From, #pstate{}=State) ->
    do_stop(State),
    gen_server:reply(From,ok),
    {noreply,stopping};

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'init',Ref}, #pstate{ref=Ref}=State) ->
    do_init(State);

handle_cast('stop', #pstate{}=State) ->
    do_stop(State),
    {noreply,stopping};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({'reinit',Ref}, #pstate{ref=Ref}=State) ->
    do_init(State);

handle_info({Port,{data,Data}}, #pstate{port=Port,os_pid=OsPid,handler_pid=HPid}=State) ->
    ?LOG('$trace',"~p data: ~120tp",[OsPid,Data]),
    HPid ! {OsPid,{data,Data}},
    {noreply,State};

handle_info({Port,{exit_status,ExitStatus}}, #pstate{port=Port,os_pid=OsPid,handler_pid=HPid}=State) ->
    ?LOG('$trace',"~p exit_status: ~120tp",[OsPid,ExitStatus]),
    HPid ! {OsPid,{exit_status,ExitStatus}},
    {noreply,State#pstate{last_exit_status=ExitStatus}};

handle_info({Port,eof}, #pstate{port=Port,os_pid=OsPid,handler_pid=HPid}=State) ->
    ?LOG('$info',"~p eof",[OsPid]),
    HPid ! {OsPid,eof},
    on_eof(State);

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------------------------
%% Init and re-init
%% ---------------------------------------------------
do_init(#pstate{mservice=Item,handler_pid=HPid}=State) ->
    CmdLine = maps:get('cmdline',Item),
    Code = maps:get('code',Item),
    Name = maps:get('name',Item),
    WorkDir = maps:get('workdir',Item),
    ?LOG('$trace',"Opening port for '~ts' '~ts' (~ts)...",[Code, Name, CmdLine]),
    case catch erlang:open_port({spawn,?BU:to_list(CmdLine)},[binary,exit_status,stream,eof,stderr_to_stdout,{cd,WorkDir}]) of
        {'EXIT',Reason} ->
            ?LOG('$error',"Open port crashed: ~120tp",[Reason]),
            {error,Reason};
        {error,_}=Err ->
            ?LOG('$error',"Open port result: ~120tp",[Err]),
            Err;
        Port when is_port(Port) ->
            {os_pid,OsPid} = erlang:port_info(Port,os_pid),
            PidFilePath = maps:get('pidfile_path',Item),
            file:write_file(PidFilePath,?BU:to_binary(OsPid)),
            ?LOG('$info',"~ts. '~ts' '~ts' Process started: ~120tp",[?APP,Code,Name,OsPid]),
            ?BLmonitor:start_monitor(self(), [fun() -> kill_process(OsPid,PidFilePath,true) end, fun() -> close_port(Port) end]),
            State1 = State#pstate{port=Port, os_pid=OsPid},
            HPid ! {started,OsPid},
            {noreply,State1}
    end.

%% ---------------------------------------------------
%% When external stop (takeover or mservice deleted)
%% ---------------------------------------------------
do_stop(#pstate{regname=RegName,code=Code,port=Port,os_pid=OsPid,mservice=Item}) ->
    PidFilePath = maps:get('pidfile_path',Item),
    kill_process(OsPid,PidFilePath,false),
    close_port(Port),
    ?LOG('$trace',"Process killed: ~120tp",[OsPid]),
    ?BLmonitor:stop_monitor(self()),
    spawn(fun() ->
              ?FSUPV:terminate_child(Code,RegName),
              ?FSUPV:delete_child(Code,RegName)
          end),
    ok.

%% @private
kill_process(OsPid,PidFilePath,Async) ->
    ?LOG('$trace',"Killing process ~p...", [OsPid]),
    F = fun() ->
            case erlang:open_port({spawn,?BU:str("kill ~p",[OsPid])}, [binary,stream,stderr_to_stdout]) of
                {error,_}=Err ->
                    ?LOG('$error',"Kill process error (os_pid=~p): ~120tp",[OsPid,Err]), ok;
                Port when is_port(Port) ->
                    case read_pidfile(PidFilePath) of
                        {error,enoent} ->
                            ?LOG('$error',"Pidfile not found (os_pid=~p)",[OsPid]), ok;
                        {error,invalid_data} ->
                            ?LOG('$error',"Pidfile contains invalid data (os_pid=~p)",[OsPid]),
                            case file:delete(PidFilePath) of
                                ok -> ok;
                                {error,_}=Err ->
                                    ?LOG('$error',"Pidfile delete error (os_pid=~p): ~120tp",[OsPid,Err]), ok
                            end;
                        {ok,OsPid} ->
                            case file:delete(PidFilePath) of
                                ok -> ok;
                                {error,_}=Err ->
                                    ?LOG('$error',"Pidfile delete error (os_pid=~p): ~120tp",[OsPid,Err]), ok
                            end;
                        {ok,OsPid2} ->
                            ?LOG('$error',"Pidfile contains wrong data. Expected os_pid=~p, Found os_pid=~p",[OsPid,OsPid2]), ok
                    end end end,
    case Async of
        true -> spawn(F);
        false -> F()
    end.

%% @private
read_pidfile(PidFilePath) ->
    case file:read_file(PidFilePath) of
        {error,enoent} -> {error,enoent};
        {ok,Data} ->
            case ?BU:to_int(Data,undefined) of
                undefined -> {error,invalid_data};
                OsPid -> {ok,OsPid}
            end end.

%% @private
close_port(Port) ->
    catch erlang:port_close(Port).

%% ---------------------------------------------------
%% When os process found down
%% ---------------------------------------------------
on_eof(#pstate{port=Port,mservice=Item,last_exit_ts=LETS,last_exit_status=LES}=State) ->
    NowTS = ?BU:timestamp(),
    LETS1 = [NowTS|LETS],
    LETS2 = case length(LETS1) of
               N when N > 10 -> lists:droplast(LETS1);
               _ -> LETS1
           end,
    close_port(Port),
    file:delete(maps:get('pidfile_path',Item)),
    State1 = State#pstate{port=undefined,
                          os_pid=undefined,
                          last_exit_ts=LETS2},
    case maps:get('restart_mode',Item,'permanent') of
                  'temporary' -> to_disable(State1);
                  'transient' when LES==0 -> to_disable(State1);
                  'transient' -> to_restart(State1);
                  'permanent' -> to_restart(State1)
    end.

%% @private
to_restart(#pstate{last_exit_ts=LETS,ref=Ref}=State) ->
    NowTS = ?BU:timestamp(),
    case LETS of
        [_,_,_,_,TS5|_] when NowTS - TS5 < 10000 ->
            ?LOG('$trace',"Last 5 starts terminated in 10 sec. Pausing for 5 sec..."),
            erlang:send_after(5000,self(),{'reinit',Ref}),
            {noreply,State};
        [_,_,_,TS4|_] when NowTS - TS4 < 10000 ->
            ?LOG('$trace',"Last 4 starts terminated in 10 sec. Pausing for 5 sec..."),
            erlang:send_after(3000,self(),{'reinit',Ref}),
            {noreply,State};
        [_,_,TS3|_] when NowTS - TS3 < 10000 ->
            ?LOG('$trace',"Last 3 starts terminated in 10 sec. Pausing for 1 sec..."),
            erlang:send_after(1000,self(),{'reinit',Ref}),
            {noreply,State};
        _ ->
            do_init(State)
    end.

%% @private
to_disable(#pstate{mservice=Item}=State) ->
    ?BLmonitor:stop_monitor(self()),
    % terminate port server
    spawn(fun() -> ?PortSrv:stop(Item) end),
    {noreply,State}.
