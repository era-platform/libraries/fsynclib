%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.07.2021
%%% @doc

-module(fsynclib_folder_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([get_regname/1]).

-export([sync_freeze/3,
         wait_sync_queue_flushed/3,
         get_sync_info/2,
         get_cache_data/2,
         get_history_for/2]).

-export([apply_sync_event/2]).

-compile({no_auto_import,[size/1]}).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(RegName, Opts) ->
    gen_server:start_link({local, RegName}, ?MODULE, Opts#{'regname' => RegName}, []).

get_regname(Code) when is_binary(Code); is_atom(Code) ->
    ?BU:to_atom_new(?BU:strbin("~ts:srv;code:~ts",[?APP,Code])).

%% -----------------
sync_freeze(Code,FromNode,Mode) ->
    gen_server:call(get_regname(Code), {sync_freeze,FromNode,Mode}).

%% -----------------
wait_sync_queue_flushed(Code,FromNode,Timeout) ->
    gen_server:call(get_regname(Code), {wait_sync_queue_flushed,FromNode,?BU:timestamp(),Timeout}).

%% -----------------
get_sync_info(Code,Args) when is_binary(Code); is_atom(Code) ->
    gen_server:call(get_regname(Code), {get_sync_info,Args}).

%% -----------------
get_cache_data(Code,Args) when is_binary(Code); is_atom(Code) ->
    gen_server:call(get_regname(Code), {get_cache_data,Args}).

%% -----------------
get_history_for(Code,Args) when is_binary(Code); is_atom(Code) ->
    gen_server:call(get_regname(Code), {get_history_for,Args}).

%% -----------------
apply_sync_event(Code,Event) when is_binary(Code); is_atom(Code) ->
    gen_server:call(get_regname(Code), {apply_sync_event,Event}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [RegName, Code, RootDir, Nodes, Groups] = ?BU:extract_required_props(['regname','code','path','nodes','groups'], Opts),
    Ref = make_ref(),
    State = #fstate{regname = RegName,
                    code = Code,
                    path = string:trim(RootDir, trailing, "/"),
                    nodes = lists:sort(Nodes--[node()]),
                    groups = Groups,
                    self = self(),
                    ref = Ref,
                    state = '$initial',
                    initts = ?BU:timestamp()},
    gen_server:cast(self(), {'init',Ref}),
    ?LOG('$info', "~ts. '~ts' facade inited", [?APP, Code]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
%% --------------
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
%% --------------
handle_call({print}, _From, #fstate{code=Code}=State) ->
    ?LOG('$force', "~ts. '~ts' facade state: ~n\t~120tp", [?APP, Code, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
%% --------------
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% setup freeze events mode
%% --------------
handle_call({sync_freeze,FromNode,{freeze,Timeout}}, _From, State) ->
    case ?Sync:freeze_start(FromNode,Timeout,State) of
        {ok,State1} -> {reply, ok, State1};
        {error,_}=Err -> {reply,Err,State}
    end;

%%
handle_call({sync_freeze,FromNode,cancel}, _From, State) ->
    case ?Sync:freeze_cancel(FromNode,State) of
        {ok,State1} -> {reply,ok,State1};
        {error,_}=Err -> {reply,Err,State}
    end;

%% --------------
%% cyclic over queue wait for sync_queue flushed
%% --------------
handle_call({wait_sync_queue_flushed,_FromNode,_FromTS,_Timeout}, _From, #fstate{sync_queue=[]}=State) ->
    {reply,ok,State};
%%
handle_call({wait_sync_queue_flushed,FromNode,FromTS,Timeout}, From, State) ->
    gen_server:cast(self(), {wait_sync_queue_flushed,{FromNode,FromTS,Timeout},From}),
    {noreply,State};

%% --------------
%% return sync info
%% --------------
handle_call({get_sync_info,Args}, _From, State) ->
    {Result,State1} = ?Sync:get_sync_info(Args,State),
    {reply,Result,State1};

%% --------------
%% return cache data
%% --------------
handle_call({get_cache_data,Args}, _From, State) ->
    {Result,State1} = ?Sync:get_cache_data(Args,State),
    {reply,Result,State1};

%% --------------
%% return history data for selected args
%% --------------
handle_call({get_history_for,Args}, _From, State) ->
    {Result,State1} = ?Sync:get_history_for(Args,State),
    {reply,Result,State1};

%% --------------
%% apply sync event from other synchronized node
%% --------------
handle_call({apply_sync_event,EventInfo}, _From, State) ->
    ?Event:apply_sync_event(EventInfo,_From,State);

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'init',Ref}, #fstate{ref=Ref}=State) ->
    State1 = do_start(State),
    {noreply,State1};

%% --------------
%% wait_sync_queue_flushed on cycled over-queue wait
%% --------------
handle_cast({wait_sync_queue_flushed,_,From}, #fstate{sync_queue=[]}=State) ->
    gen_server:reply(From,ok),
    {noreply,State};
%%
handle_cast({wait_sync_queue_flushed,{FromNode,FromTS,Timeout},From}, State) ->
    case ?BU:timestamp() of
        NowTS when NowTS-FromTS > Timeout ->
            gen_server:reply(From,{error,not_flushed});
        _ ->
            gen_server:cast(self(),{wait_sync_queue_flushed,{FromNode,FromTS,Timeout},From})
    end,
    {noreply,State};

%% --------------
%% other
%% --------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% Timer to sync other nodes
%% --------------
handle_info({timer_sync,Ref}, #fstate{tref=Ref,self=Self}=State) ->
    Ref1 = make_ref(),
    State1 = ?Sync:check_sync(State, fun(Reply) -> Self ! {sync_result,Ref1,Reply} end),
    {noreply, State1#fstate{tref = Ref1,
                            timerref = undefined}};

%% --------------
%% Sync async result
%% --------------
handle_info({sync_result,Ref,Result}, #fstate{tref=Ref}=State) ->
    State1 = ?Sync:on_sync_result(Result,State),
    Ref1 = make_ref(),
    Timeout0 = ?CFG:resync_interval(),
    Timeout = Timeout0 + ?BU:random(?BU:to_int(Timeout0 * 1.1)),
    State2 = State1#fstate{tref = Ref1,
                           timerref = erlang:send_after(Timeout,self(),{timer_sync,Ref1})},
    {noreply, State2};

%% --------------
%% apply function to state from internal modules
%% --------------
handle_info({apply,Fun}, State) when is_function(Fun,1) ->
    {noreply,Fun(State)};

%% --------------
%% inotifywait pid started
%% --------------
handle_info({started,OsPid}, State) ->
    {noreply, State#fstate{ospid=OsPid,
                           state='$started'}};

%% --------------
%% EOF from inotifywait
%% --------------
handle_info({OsPid,eof}, #fstate{ospid=OsPid,timerref=TimerRef}=State) ->
    ?BU:cancel_timer(TimerRef),
    {noreply, State#fstate{state='$exited'}};

%% --------------
%% exit_status from inotifywait
%% --------------
handle_info({OsPid, {exit_status,_ExitStatus}}, #fstate{ospid=OsPid}=State) ->
    {noreply, State#fstate{state='$exited'}};

%% --------------
%% Data from inotifywait
%% --------------
handle_info({OsPid, {data,Data}}, #fstate{ospid=OsPid}=State) ->
    Lines = binary:split(Data,?RN,[global,trim_all]),
    State1 = apply_lines(Lines, State),
    {noreply, State1};

%% --------------
%% fsync iteration finished
%% --------------
handle_info({fsync_reply,SyncRef,{ok,0}}, #fstate{sync_ref=SyncRef}=State) ->
    {noreply, State};
handle_info({fsync_reply,SyncRef,{ok,_}}, #fstate{sync_ref=SyncRef}=State) ->
    State1 = ?FldUtils:update_checksum(e,State),
    % forcely sync to actualize sync_nodes
    DoResync = ?CFG:resync_immediately_after_fsync_result(),
    State2 = case State1 of
                 #fstate{timerref=undefined} -> State1;
                 #fstate{tref=Ref,timerref=TimerRef} when DoResync ->
                     ?BU:cancel_timer(TimerRef),
                     State1#fstate{timerref = erlang:send_after(1000, self(), {timer_sync,Ref})};
                 _ -> State1
             end,
    {noreply, State2};

%% --------------
%% forcely sync one node (when it found in actual and another hc)
%% --------------
handle_info({force_sync_node,Node}, #fstate{code=_Code,nodes=Nodes,sync_nodes=SyncNodes}=State) ->
    State1 = case ?CFG:force_sync_node() of
                 false -> State;
                 true ->
                    case {lists:member(Node,Nodes), lists:member(Node,SyncNodes)} of
                        {true,false} ->
                            ?LOG('$trace',"~ts. '~ts'. force fsync node: '~ts'",[?APP,_Code,Node]),
                            ?SyncFlow:start_sync([Node],State);
                        _ -> State
                    end end,
    {noreply, State1};

%% --------------
%% other
%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #fstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

do_start(#fstate{code=Code,path=RootDir}=State) ->
    kill_zombies_by_pidfiles(State),
    filelib:ensure_dir(filename:join(RootDir,"dummy")),
    setup_max_user_watches(),
    {ok,CWD} = file:get_cwd(),
    Name = <<"inotifywait">>,
    Mservice = #{code => Code,
                 name => Name,
                 cmdline => ?BU:strbin("inotifywait -rm --format \"FSYNCLIB~ts%~tse~ts%w~ts%f\" -e delete -e create -e move -e close_write -e attrib -e moved_from -e moved_to ~ts",[?Sep,?ESep,?Sep,?Sep,RootDir]), % -e modify
                 workdir => CWD,
                 restart_mode => permanent,
                 pidfile_path => filename:join([CWD,"pidfiles",?BU:to_list(Code),?BU:str("~ts.pid",[Name])]),
                 handler_pid => self()},
    start_mservice(Mservice),
    State#fstate{state='$initial'}.

%% @private
kill_zombies_by_pidfiles(#fstate{code=Code}=State) ->
    {ok,CWD} = file:get_cwd(),
    Dir = filename:join([CWD,"pidfiles",?BU:to_list(Code)]),
    filelib:ensure_dir(filename:join(Dir,"dummy")),
    os:cmd(?BU:str("cd ~ts && find . -name '*.pid' | xargs cat | xargs kill ",[Dir])),
    os:cmd(?BU:str("cd ~ts && find . -name '*.pid' | xargs rm ",[Dir])),
    State.

%% @private
setup_max_user_watches() ->
    File0 = "/etc/sysctl.conf",
    File = "/proc/sys/fs/inotify/max_user_watches",
    case ?CFG:max_user_watches(?DefaultMaxUserWatches) of
        0 -> ok;
        MaxUserWatches ->
            Fset = fun() ->
                        file:write_file(File,?BU:to_binary(MaxUserWatches)),
                        os:cmd(?BU:str("sed -i \"s|^#*\s*fs\.inotify\.max_user_watches\s*=.*|fs.inotify.max_user_watches = ~p|g\" ~ts",[MaxUserWatches,File0])),
                        case file:read_file(File0) of
                            {ok,Data} ->
                                case binary:match(Data,<<"fs.inotify.max_user_watches">>) of
                                    {_,_} -> ok;
                                    nomatch -> os:cmd(?BU:str("echo \"\nfs.inotify.max_user_watches = ~p\" >> ~ts",[MaxUserWatches,File0]))
                                end;
                            _ -> os:cmd(?BU:str("echo \"\nfs.inotify.max_user_watches = ~p\" >> ~ts",[MaxUserWatches,File0]))
                        end
                        %os:cmd("sysctl -p --system")
                   end,
            case file:read_file(File) of
                {error,enoent} -> Fset();
                {error,_} -> ok;
                {ok,Data} ->
                    case ?BU:to_int(Data,0) of
                        I when I < MaxUserWatches -> Fset();
                        _ -> ok
                    end end end.

%% --------------------------------------------------
%% @private
stop_mservice(Item) ->
    Code = maps:get('code',Item),
    Name = maps:get('name',Item),
    ?LOG('$trace',"'~ts' Stopping mservice '~ts'...", [Code,Name]),
    catch ?PortSrv:stop(Item).

%% @private
start_mservice(Item) ->
    % last iteration could remain active mservice and return with error
    case stop_mservice(Item) of
        {ok,Pid} when is_pid(Pid) ->
            MonRef = erlang:monitor(process,Pid),
            receive {'DOWN',MonRef,process,Pid,_} -> timer:sleep(100)
            after 3000 -> ok
            end;
        _ -> ok
    end,
    ?PortSrv:start(Item).

%% --------------------------------------------------
apply_lines(Lines, State) ->
    lists:foldl(fun(Line,Acc) -> parse_line(Line,Acc) end, State, Lines).

%% @private
parse_line(Line,#fstate{code=Code}=State) ->
    case Line of
        <<"Watches established.">> ->
            ?LOG('$info',"~ts. '~ts' Watches established. Loading caches...",[?APP,Code]),
            State1 = load_data(State),
            Ref = make_ref(),
            ?LOG('$info',"~ts. '~ts' Running...",[?APP,Code]),
            State1#fstate{state = '$running',
                          tref = Ref,
                          timerref = erlang:send_after(0,self(),{timer_sync,Ref})};
        <<"FSYNCLIB",Rest/binary>> ->
            [Event,Dir,Filename] = case binary:split(Rest, [?Sep,?RN], [global,trim_all]) of
                                       [A,B,C] -> [A,B,C];
                                       [A,B] -> [A,B,<<>>] % ATTRIB|ISDIR has only dir
                                   end,
            Events = binary:split(Event,?ESep,[global,trim_all]),
            ?LOG('$trace',"Events: ~120tp, Dir: ~120tp, Name: ~120tp",[Events,Dir,Filename]),
            ?Event:apply_data(Events,Dir,Filename,State);
        _ ->
            State
    end.

%% --------------------------------------------------
load_data(State) ->
    State1 = load_caches(State),
    load_history(State1).

%%
load_caches(#fstate{code=_Code,path=RootDir}=State) ->
    H1 = ?Dict:delete(?CacheBuilder:build_h1(?BU:to_unicode_list(RootDir)),RootDir), % TODO: refresh by previous
    ?LOG('$info',"~ts. '~ts' Cache loaded ~p items.",[?APP,_Code,?Dict:size(H1)]),
    ?FldUtils:update_checksum(a,State#fstate{cache_h1 = H1}).

%%
load_history(#fstate{code=Code,path=RootDir,cache_h1=H1}=State) ->
    History = ?History:load(Code,RootDir),
    History1 = ?History:refresh(Code,History,H1),
    ?LOG('$info',"~ts. '~ts' History loaded ~p items.",[?APP,Code,dets:info(History1,size)]),
    State#fstate{history = History1}.

