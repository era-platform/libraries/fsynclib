%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.07.2021
%%% @doc Cache H1 routines on events handling

-module(fsynclib_folder_event_cache).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    cache_file_modified/3,
    cache_file_deleted/3,
    cache_file_moved/4,
    cache_file_attr/3,
    cache_dir_created/3,
    cache_dir_deleted/3,
    cache_dir_moved/4
    % cache_dir_attr/3 % DIR_MTIME: directory doesn't need mtime check
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

cache_file_modified(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
    Info = ?CacheBuilder:build_info_h1(AbsPath),
    #file_info{size=Size}=?CacheBuilder:get_info(Info),
    ?Dict:put(H1,{AbsPath,Info}),
    ?History:push(AbsPath, {modified,'regular',EventTS,[Size]}, State),
    State#fstate{hc = undefined}.

cache_file_deleted(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
    ?Dict:delete(H1,AbsPath),
    ?History:push(AbsPath, {deleted,'regular',EventTS,[]}, State),
    State#fstate{hc = undefined}.

cache_file_moved(FromPath,ToPath,EventTS,#fstate{cache_h1=H1}=State) ->
    Size = case ?Dict:find(H1,FromPath,undefined) of
               undefined -> 0;
               Info -> (?CacheBuilder:get_info(Info))#file_info.size
           end,
    ?Dict:delete(H1,FromPath),
    ?Dict:put(H1,{ToPath,?CacheBuilder:build_info_h1(ToPath)}),
    ?History:push(FromPath, {deleted,'regular',EventTS,[]}, State),
    ?History:push(ToPath, {modified,'regular',EventTS,[Size]}, State),
    State#fstate{hc = undefined}.

cache_file_attr(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
    Info = ?CacheBuilder:build_info_h1(AbsPath),
    #file_info{size=Size} = ?CacheBuilder:get_info(Info),
    ?Dict:put(H1,{AbsPath,Info}),
    ?History:push(AbsPath, {modified,'regular',EventTS,[Size]}, State),
    State#fstate{hc = undefined}.

cache_dir_created(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
    Info = ?CacheBuilder:build_info_h1(AbsPath),
    #file_info{size=Size}=?CacheBuilder:get_info(Info),
    ?Dict:put(H1,{AbsPath,Info}),
    ?History:push(AbsPath, {modified,'directory',EventTS,[Size]}, State),
    State#fstate{hc = undefined}.

cache_dir_deleted(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
    ?Dict:delete(H1,AbsPath), % inotify recursively events on every item inside deleted directory
    ?History:push(AbsPath, {deleted,'directory',EventTS,[]}, State),
    State#fstate{hc = undefined}.

cache_dir_moved(FromPath,ToPath,EventTS,#fstate{cache_h1=H1}=State) ->
    % cache
    ?Dict:delete(H1,FromPath),
    FromChildren = ?FldUtils:find_children(FromPath,H1),
    ?Dict:fold(fun({K,_V},_) -> ?Dict:delete(H1,K) end, ok, FromChildren),
    ?Dict:fold(fun({K,V},_) -> ?Dict:put(H1,{K,V}) end, ok, SubH1 = ?CacheBuilder:build_h1(ToPath)),
    % history del
    ?History:push(FromPath, {deleted,'directory',EventTS,[]}, State),
    ?Dict:fold(fun({K,V},_) ->
                        case ?CacheBuilder:get_info(V) of
                            #file_info{type='regular'} -> ?History:push(K, {deleted,'regular',EventTS,[]}, State);
                            #file_info{type='directory'} -> ?History:push(K, {deleted,'directory',EventTS,[]}, State)
                        end end, ok, FromChildren),
    % history add
    ?Dict:fold(fun({K,V},_) ->
                    Size = case ?Dict:find(H1,K,undefined) of
                               undefined -> 0;
                               Info -> (?CacheBuilder:get_info(Info))#file_info.size
                           end,
                    case ?CacheBuilder:get_info(V) of
                        #file_info{type='regular'} -> ?History:push(K, {modified,'regular',EventTS,[Size]}, State);
                        #file_info{type='directory'} -> ?History:push(K, {modified,'directory',EventTS,[Size]}, State)
                    end end, ok, SubH1),
    ?Dict:close(SubH1),
    State#fstate{hc = undefined}.

%%%% DIR_MTIME: directory doesn't need mtime check
%%cache_dir_attr(AbsPath,EventTS,#fstate{cache_h1=H1}=State) ->
%%    Info = ?CacheBuilder:build_info_h1(AbsPath),
%%    #file_info{size=Size} = ?CacheBuilder:get_info(Info),
%%    ?Dict:put(H1,{AbsPath,Info}),
%%    ?History:push(AbsPath, {modified,'directory',EventTS,[Size]}, State),
%%    State#fstate{hc = undefined}.

%% ====================================================================
%% Internal functions
%% ====================================================================