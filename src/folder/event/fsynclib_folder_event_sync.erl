%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.07.2021
%%% @doc Routines to apply sync_changes notify events from other node

-module(fsynclib_folder_event_sync).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sync_changes/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

sync_changes([FromNode,ToApply],_From,#fstate{code=Code,path=RootDir,sync_queue=Q}=State) ->
    ?LOG('$trace',"~ts. '~ts'. got sync_changes from '~ts' (~p items)",[?APP,Code,FromNode,length(ToApply)]),
    ?LOG('$debug',"~ts. '~ts'. got sync_changes from '~ts' items: ~n\t~120tp",[?APP,Code,FromNode,ToApply]),
    Fget = fun(Key,OptsMap) -> maps:get(Key,OptsMap) end,
    Fpath = fun(OptsMap) -> ?FldUtils:abs_path(RootDir,Fget(rel_path,OptsMap)) end,
    Ffrompath = fun(OptsMap) -> ?FldUtils:abs_path(RootDir,Fget(from_rel_path,OptsMap)) end,
    ToApply1 = lists:map(fun({{{file,modified},_,_},OptsMap}) -> {sync_file_modified,[Fpath(OptsMap),OptsMap]};
                            ({{{file,deleted},_,_},OptsMap}) -> {sync_file_deleted,[Fpath(OptsMap),OptsMap]};
                            ({{{file,{moved,_}},_,_},OptsMap}) -> {sync_file_moved,[Fpath(OptsMap),Ffrompath(OptsMap),OptsMap]};
                            ({{{file,attr},_,_},OptsMap}) -> {sync_file_attr,[Fpath(OptsMap),OptsMap]};
                            ({{{dir,created},_,_},OptsMap}) -> {sync_dir_created,[Fpath(OptsMap),OptsMap]};
                            ({{{dir,deleted},_,_},OptsMap}) -> {sync_dir_deleted,[Fpath(OptsMap),OptsMap]};
                            ({{{dir,{moved,_}},_,_},OptsMap}) -> {sync_dir_moved,[Fpath(OptsMap),Ffrompath(OptsMap),OptsMap]};
                            % ({{{dir,attr},_,_},OptsMap}) -> {sync_dir_attr,[Fpath(OptsMap),OptsMap]} % DIR_MTIME: directory doesn't need mtime check
                            % --- forwarded
                            ({sync_file_modified,OptsMap}) -> {sync_file_modified,[Fpath(OptsMap),OptsMap]};
                            ({sync_file_deleted,OptsMap}) -> {sync_file_deleted,[Fpath(OptsMap),OptsMap]};
                            ({sync_file_moved,OptsMap}) -> {sync_file_moved,[Fpath(OptsMap),Ffrompath(OptsMap),OptsMap]};
                            ({sync_file_attr,OptsMap}) -> {sync_file_attr,[Fpath(OptsMap),OptsMap]};
                            ({sync_dir_created,OptsMap}) -> {sync_dir_created,[Fpath(OptsMap),OptsMap]};
                            ({sync_dir_deleted,OptsMap}) -> {sync_dir_deleted,[Fpath(OptsMap),OptsMap]};
                            ({sync_dir_moved,OptsMap}) -> {sync_dir_moved,[Fpath(OptsMap),Ffrompath(OptsMap),OptsMap]}
                         end, ToApply),
    case Q of
        [] ->
            FunDequeue = fun(StateF) -> sync_dequeue(StateF) end,
            self() ! {apply,FunDequeue},
            ok;
        _ -> ok
    end,
    {ok,State#fstate{sync_queue = Q ++ [{FromNode,Task} || Task <- ToApply1]}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% dequeue other node events one by one (avoid long-running iteration)
sync_dequeue(#fstate{sync_queue=[],hc=undefined}=State) -> ?FldUtils:update_checksum(d,State);
sync_dequeue(#fstate{sync_queue=[]}=State) -> State;
sync_dequeue(#fstate{sync_queue=[Task|Rest]}=State) ->
    State1 = perform_sync_task(Task,State),
    FunDequeue = fun(StateF) -> sync_dequeue(StateF) end,
    self() ! {apply,FunDequeue},
    State1#fstate{sync_queue=Rest}.

%% @private
perform_sync_task({FromNode,{Method,Args}},#fstate{code=Code}=State) ->
    ?LOG('$trace',"~ts. '~ts'. perform sync event from ~ts: ~ts(~1000tp)",[?APP,Code,FromNode,Method,Args]),
    State1 = case Method of
                 sync_file_modified -> sync_file_modified(FromNode,Args,State);
                 sync_file_deleted -> sync_file_deleted(FromNode,Args,State);
                 sync_file_moved -> sync_file_moved(FromNode,Args,State);
                 sync_file_attr -> sync_file_attr(FromNode,Args,State);
                 sync_dir_created -> sync_dir_created(FromNode,Args,State);
                 sync_dir_deleted -> sync_dir_deleted(FromNode,Args,State);
                 sync_dir_moved -> sync_dir_moved(FromNode,Args,State)
                 % sync_dir_attr -> sync_dir_attr(FromNode,Args,State) % DIR_MTIME: directory doesn't need mtime check
             end,
    forward_to_groups(FromNode,{Method,Args},State1).

%% @private
forward_to_groups(_,_,#fstate{groups=[]}=State) -> State;
forward_to_groups(_,_,#fstate{groups=[_]}=State) -> State;
forward_to_groups(FromNode,{Method,Args},#fstate{code=Code,path=RootDir,groups=Groups}=State) ->
    SyncNodes0 = lists:usort(lists:flatten(lists:filter(fun(Group) -> not lists:member(FromNode,Group) end,Groups))),
    SyncNodes = SyncNodes0 -- lists:usort(lists:flatten(lists:filter(fun(Group) -> lists:member(FromNode,Group) end, Groups))),
    ?LOG('$trace',"~ts. '~ts'. forward sync event to nodes: ~120tp",[?APP,Code,SyncNodes]),
    OptsMap = lists:last(Args),
    OptsMap1 = OptsMap#{abs_path => ?FldUtils:abs_path(RootDir,maps:get(rel_path,OptsMap))},
    ToApply = [{Method,OptsMap1}],
    EventInfo = {sync_changes,[node(),ToApply]},
    _Res = ?BLmulticall:call_direct(SyncNodes,{?FSRV,apply_sync_event,[Code,EventInfo]},5000),
    State.

%% ----------------------------------
%% Private other node event handlers
%% ----------------------------------
sync_file_modified(FromNode,[LocalAbsPath,OptsMap],State) ->
    RemoteAbsPath = maps:get(abs_path,OptsMap),
    MTime = maps:get(mtime,OptsMap),
    Size = maps:get(size,OptsMap),
    Skip = case ?BLfile:read_file_info(LocalAbsPath) of
               {ok,#file_info{mtime=MTime,size=Size}} -> true;
               _ -> false
           end,
    case Skip of
        true -> State;
        false ->
            ok = ?BLfilecopier:copy_file(FromNode, RemoteAbsPath, LocalAbsPath, fun(Level,Fmt,Args) -> ?LOG(Level,Fmt,Args) end, ?CFG:copier_bandwidth()),
            %?BLfile:change_time(LocalAbsPath,MTime),
            EventTS = maps:get(event_ts,OptsMap),
            ?EventCache:cache_file_modified(LocalAbsPath,EventTS,State)
    end.

sync_file_deleted(_FromNode,[LocalAbsPath,OptsMap],State) ->
    ?BLfile:delete(LocalAbsPath),
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_file_deleted(LocalAbsPath,EventTS,State).

sync_file_moved(_FromNode,[ToLocalAbsPath,FromLocalAbsPath,OptsMap],State) ->
    ?BLfile:rename(FromLocalAbsPath,ToLocalAbsPath),
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_file_moved(FromLocalAbsPath,ToLocalAbsPath,EventTS,State).

sync_file_attr(_FromNode,[LocalAbsPath,OptsMap],State) ->
    MTime = maps:get(mtime,OptsMap),
    ?BLfile:change_time(LocalAbsPath,MTime),
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_file_attr(LocalAbsPath,EventTS,State).

sync_dir_created(_FromNode,[LocalAbsPath,OptsMap],State) ->
    ?BLfilelib:ensure_dir(filename:join(LocalAbsPath,".")),
    % ?BLfile:change_time(LocalAbsPath, maps:get(mtime,OptsMap)), % DIR_MTIME: directory doesn't need mtime check
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_dir_created(LocalAbsPath,EventTS,State).

sync_dir_deleted(_FromNode,[LocalAbsPath,OptsMap],State) ->
    ?BU:directory_delete(LocalAbsPath),
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_dir_deleted(LocalAbsPath,EventTS,State).

sync_dir_moved(_FromNode,[ToLocalAbsPath,FromLocalAbsPath,OptsMap],State) ->
    ?BLfile:rename(FromLocalAbsPath,ToLocalAbsPath),
    EventTS = maps:get(event_ts,OptsMap),
    ?EventCache:cache_dir_moved(FromLocalAbsPath,ToLocalAbsPath,EventTS,State).

%% %% DIR_MTIME: directory doesn't need mtime check
%%sync_dir_attr(_FromNode,[LocalAbsPath,OptsMap],State) ->
%%    MTime = maps:get(mtime,OptsMap),
%%    ?BLfile:change_time(LocalAbsPath,MTime),
%%    EventTS = maps:get(event_ts,OptsMap),
%%    ?EventCache:cache_file_attr(LocalAbsPath,EventTS,State).
