%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.07.2021
%%% @doc Routines to apply dequeued group of events from inotifywait

-module(fsynclib_folder_event_apply).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

apply(ToApply,State) ->
    apply_changes(
        filter_doublicated(
            filter_only_regulars_and_directories(
                filter_already_applied(
                    transform_moved(
                        multiply_dir_created(
                            filter_file_modified(ToApply,State),State),State),State),State),State),State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------
%% @private
filter_file_modified(ToApply,#fstate{code=_Code,fs_queue=Q}) ->
    ?LOG('$debug',"~ts. '~ts'. filter_file_modified <- ~p events",[?APP,_Code,length(ToApply)]),
    F = fun F([],Acc) -> lists:reverse(Acc);
            F([{{file,modified},AbsPath,TS}=Item|Rest],Acc) ->
                case lists:keyfind(AbsPath,2,Rest) of
                    {{file,modified},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                    {{file,deleted},_,_} -> F(Rest,Acc);
                    {_,_,_} -> F(Rest,[Item|Acc]);
                    false ->
                        case lists:keyfind(AbsPath,2,Q) of
                            {{file,modified},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                            {{file,deleted},_,_} -> F(Rest,Acc);
                            _ -> F(Rest,[Item|Acc])
                        end
                end;
            F([{{EvBase,attr},AbsPath,TS}=Item|Rest],Acc) ->
                case lists:keyfind(AbsPath,2,Rest) of
                    {{EvBase,attr},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                    {{EvBase,modified},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                    {{EvBase,deleted},_,_} -> F(Rest,Acc);
                    {_,_,_} -> F(Rest,[Item|Acc]);
                    false ->
                        case lists:keyfind(AbsPath,2,Q) of
                            {{EvBase,attr},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                            {{EvBase,modified},_,TSx} when TSx-TS < ?SkipModifiedTimeout -> F(Rest,Acc);
                            {{EvBase,deleted},_,_} -> F(Rest,Acc);
                            _ -> F(Rest,[Item|Acc])
                        end
                end;
            F([Item|Rest],Acc) -> F(Rest,[Item|Acc])
        end,
    F(ToApply,[]).

%% --------
%% @private
multiply_dir_created(ToApply,#fstate{code=_Code,cache_h1=_H1}) ->
    ?LOG('$debug',"~ts. '~ts'. multiply_dir_created <- ~p events",[?APP,_Code,length(ToApply)]),
    F = fun F([],Acc) -> lists:reverse(Acc);
            F([{{dir,created},AbsPath,TS}=Item|Rest],Acc) ->
                    % COMMENTED, because dir deleted && dir created before freeze time - filtered, and cache is not updated by dir deleted
                    %case check_in_cache(AbsPath,H1) andalso not check_in_portion(AbsPath,Acc) of
                    %    true -> F(Rest,Acc); % economy, but if dir deleted and created at same portion then it's a bug
                    %    false ->
                    Items = filelib:wildcard("*",AbsPath),
                    Items1 = lists:filtermap(fun(RelPath) ->
                                                    SubPath = filename:join(AbsPath,RelPath),
                                                    case filelib:is_dir(SubPath) of
                                                        false -> {true,{file,SubPath}};
                                                        true -> {true,{dir,SubPath}}
                                                    end end, Items),
                    Items2 = lists:sort(Items1),
                    NewRest1 = lists:foldr(fun({dir,SubPath},Acc1) -> [{{dir,created},SubPath,TS}|Acc1];
                                              (_,Acc1) -> Acc1
                                           end, Rest, Items2),
                    NewRest2 = lists:foldl(fun({file,SubPath},Acc3) -> [{{file,modified},SubPath,TS}|Acc3];
                                              (_,Acc3) -> Acc3
                                           end, NewRest1, Items2),
                    F(NewRest2,[Item|Acc]);
                    %end;
            % % DIR_MTIME: directory doesn't need mtime check
            %F([{{dir,attr},AbsPath,_}=Item|Rest],Acc) ->
            %    case check_attrs(AbsPath,H1) of
            %        true -> F(Rest,Acc);
            %        false -> F(Rest,[Item|Acc])
            %    end;
            F([Item|Rest],Acc) -> F(Rest,[Item|Acc])
        end,
    F(ToApply,[]).

%% --------
%% @private
%% change portion by transform moved_from, moved_to events of same inode into modified/created/deleted/{moved,FromPath}
transform_moved(ToApply,#fstate{code=_Code,cache_h1=H1}) ->
    ?LOG('$debug',"~ts. '~ts'. transform_moved <- ~p events",[?APP,_Code,length(ToApply)]),
    ToApply1 = lists:zip(lists:seq(1,length(ToApply)),ToApply),
    {Moved,Other} = lists:partition(fun({_,{{_,EvName},_,_}}) -> lists:member(EvName,['moved_from','moved_to']) end, ToApply1),
    Moved1 = lists:foldl(fun({Idx,{{_EvBase,moved_from}=Ev,AbsPath,TS}},Acc) ->
                                case ?Dict:find(H1,AbsPath,undefined) of
                                    undefined -> Acc;
                                    Data ->
                                        #file_info{inode=Inode,type=Type}=FileInfo = ?CacheBuilder:get_info(Data),
                                        [{Idx,Type,Inode,FileInfo,{Ev,AbsPath,TS}} | Acc]
                                end;
                            ({Idx,{{_EvBase,moved_to}=Ev,AbsPath,TS}},Acc) ->
                                % for filter, could be already in cache after notify sync operation
                                {INodeInCache,TypeInCache} = case ?Dict:find(H1,AbsPath,undefined) of
                                                                 undefined -> {undefined,undefined};
                                                                 Data ->
                                                                     #file_info{inode=Inode0,type=Type0} = ?CacheBuilder:get_info(Data),
                                                                     {Inode0,Type0}
                                                             end,
                                case file:read_file_info(AbsPath) of
                                    {error,_} -> Acc;
                                    {ok,#file_info{type=TypeInCache,inode=INodeInCache}} -> Acc; % filter, already in cache
                                    {ok,#file_info{type='directory'=Type,inode=Inode}=DirInfo} ->
                                        [{Idx,Type,Inode,DirInfo,{Ev,AbsPath,TS}} | Acc];
                                    {ok,#file_info{type='regular'=Type,inode=Inode}=FileInfo} ->
                                        [{Idx,Type,Inode,FileInfo,{Ev,AbsPath,TS}} | Acc]
                                end
                         end, [], Moved),
    MovedInodes = lists:foldl(fun({_,_,Inode,_,_}=Z,Acc) -> Acc#{Inode => [Z | maps:get(Inode,Acc,[])]} end, #{}, Moved1),
    Moved2 = maps:fold(fun(_,Events,Acc) ->
                            case Events of
                                [{Idx,_Type,_,FileInfo,{{EvBase,EvName},AbsPath,TS}}] ->
                                    case {EvBase,EvName} of
                                        {file,moved_from} -> [{Idx,{{file,deleted},AbsPath,TS}} | Acc];
                                        {file,moved_to} -> [{Idx,{{file,modified},AbsPath,TS}} | Acc];
                                        {dir,moved_from} ->
                                            Children = lists:reverse(lists:keysort(1,?FldUtils:find_children(AbsPath,H1))),
                                            ChildrenItems = build_children_items(Children,{Idx,TS},[{dir,deleted},{file,deleted}]), % without top dir ?
                                            ChildrenItems ++ [{Idx,{{dir,deleted},AbsPath,TS}}] ++ Acc;
                                        {dir,moved_to} ->
                                            SubH1 = ?CacheBuilder:build_h1(AbsPath,FileInfo,#{}),
                                            Children = lists:keysort(1,maps:to_list(SubH1)), % also contains top dir
                                            build_children_items(Children,{Idx,TS},[{dir,created},{file,modified}]) ++ Acc
                                    end;
                                [{Idx1,Type,_,_,{{EvBase,moved_from},AbsPath1,TS1}}, {_,Type,_,_,{{EvBase,moved_to},AbsPath2,_}}] ->
                                    case EvBase of
                                        file -> [{Idx1,{{file,{moved,AbsPath1}},AbsPath2,TS1}} | Acc];
                                        dir -> [{Idx1,{{dir,{moved,AbsPath1}},AbsPath2,TS1}} | Acc]
                                    end;
                                [{_,Type,_,_,{{EvBase,moved_to},_,_}}, {_,Type,_,_,{{EvBase,moved_from},_,_}}] ->
                                    Acc
                            end end, [], MovedInodes),
    element(2,lists:unzip(lists:keysort(1,Other++Moved2))).

%% @private
build_children_items(Children,{Idx,TS},[DirEvent,FileEvent]) ->
    Len = length(Children),
    lists:map(fun({CIdx,{CAbsPath,CInfo}}) ->
                    case ?CacheBuilder:get_info(CInfo) of
                        #file_info{type='directory'} -> {CIdx,{DirEvent,CAbsPath,TS}};
                        #file_info{type='regular'} -> {CIdx,{FileEvent,CAbsPath,TS}}
                    end end,
              lists:zip(lists:map(fun(I) -> Idx + I/(Len+1) end, lists:seq(1,Len)), Children)).

%% --------
%% @private
%% filters already applied events (got from other nodes and already applied to cache)
%% filter by match cache meta and file meta
%% filter by nonmatch operation and file meta (fix delete && create in fast way on same or different machines)
filter_already_applied(ToApply,#fstate{code=Code,cache_h1=H1}) ->
    ?LOG('$debug',"~ts. '~ts'. filter_already_applied <- ~p events",[?APP,Code,length(ToApply)]),
    FunInfos = fun(AbsPath) ->
                    CacheInfo = case ?Dict:find(H1,AbsPath,undefined) of
                                    undefined -> undefined;
                                    Item -> ?CacheBuilder:get_info(Item)
                                end,
                    DiskInfo = case ?BLfile:read_file_info(AbsPath) of
                                   {ok,#file_info{}=FileInfo} -> FileInfo;
                                   _ -> undefined
                               end,
                    {CacheInfo,DiskInfo}
               end,
    FunSameAttrs = fun(AbsPath) ->
                        case FunInfos(AbsPath) of
                            {#file_info{size=Size,mtime=MTime},#file_info{size=Size,mtime=MTime}} -> false;
                            _ -> true
                        end end,
    FunExists = fun(file,AbsPath) -> ?BLfilelib:is_regular(AbsPath);
                   (dir,AbsPath) -> ?BLfilelib:is_dir(AbsPath)
                end,
    ToApply1 = lists:filter(fun({{file,modified},AbsPath,_}) -> FunSameAttrs(AbsPath) andalso FunExists(file,AbsPath);
                               ({{file,deleted},AbsPath,_}) -> ?Dict:find(H1,AbsPath,undefined) /= undefined andalso not FunExists(file,AbsPath);
                               ({{file,{moved,FromPath}},AbsPath,_}) -> ?Dict:find(H1,FromPath,undefined) /= undefined orelse ?Dict:find(H1,AbsPath,undefined) == undefined;
                               ({{file,attr},AbsPath,_}) -> FunSameAttrs(AbsPath) andalso FunExists(file,AbsPath);
                               ({{dir,created},AbsPath,_}) -> ?Dict:find(H1,AbsPath,undefined) == undefined andalso FunExists(dir,AbsPath);
                               ({{dir,deleted},AbsPath,_}) -> ?Dict:find(H1,AbsPath,undefined) /= undefined andalso not FunExists(dir,AbsPath);
                               ({{dir,{moved,FromPath}},AbsPath,_}) -> ?Dict:find(H1,FromPath,undefined) /= undefined orelse ?Dict:find(H1,AbsPath,undefined) == undefined
                               %({{dir,attr},AbsPath,_}) -> FunSameAttrs(AbsPath) % DIR_MTIME: directory doesn't need mtime check
                            end, ToApply),
    Len = length(ToApply),
    case length(ToApply1) of
        Len -> ToApply;
        Len1 ->
            ?LOG('$trace',"~ts. '~ts'. filtered ~p/~p own events",[?APP,Code,Len-Len1,Len]),
            ToApply1
    end.

%% --------
%% @private
%% filters only events for files and directories except symlinks and hardlinks
filter_only_regulars_and_directories(ToApply,_State) ->
    F = fun(AbsPath) ->
            case file:read_file_info(AbsPath) of
                {ok,#file_info{type=T}} -> T=='regular' orelse T=='directory';
                {error,_} -> true
            end end,
    lists:filter(fun({{file,modified},AbsPath,_}) -> F(AbsPath);
                    ({{file,deleted},_AbsPath,_}) -> true;
                    ({{file,{moved,_FromPath}},AbsPath,_}) -> F(AbsPath);
                    ({{file,attr},AbsPath,_}) -> F(AbsPath);
                    ({{dir,created},AbsPath,_}) -> F(AbsPath);
                    ({{dir,deleted},_AbsPath,_}) -> true;
                    ({{dir,{moved,_FromPath}},AbsPath,_}) -> F(AbsPath)
                    %({{dir,attr},AbsPath,_}) -> F(AbsPath) % DIR_MTIME: directory doesn't need mtime check
                 end, ToApply).

%% --------
%% @private
%% filters doublicated events
filter_doublicated(ToApply,_State) ->
    {List,_} = lists:foldl(fun({A,Path,_}=Item,{Acc,Map}) ->
                                K = {A,Path},
                                case maps:get(K,Map,undefined) of
                                    undefined -> {[Item|Acc],Map#{K => true}};
                                    true -> {Acc,Map}
                                end
                           end, {[],#{}}, ToApply),
    lists:reverse(List).

%% --------
%% @private
apply_changes([],#fstate{code=Code}=State) ->
    ?LOG('$trace',"~ts. '~ts'. apply fs events skipped: nothing to apply",[?APP,Code]),
    State;
apply_changes(ToApply,#fstate{code=Code}=State) ->
    ?LOG('$trace',"~ts. '~ts'. apply fs events: ~n\t~120tp",[?APP,Code,ToApply]),
    State1 = apply_changes_to_nodes(ToApply,State),
    apply_changes_to_cache(ToApply,State1).

%% @private
apply_changes_to_nodes([],State) -> State;
apply_changes_to_nodes(_,#fstate{sync_nodes=[]}=State) -> State;
apply_changes_to_nodes(ToApply,#fstate{code=Code,path=RootDir,sync_nodes=SyncNodes}=State) ->
    Fopts = fun(AbsPath,EventTS) -> #{event_ts => EventTS,
                                      rel_path => ?FldUtils:rel_path(RootDir,AbsPath)} end,
    ToApply1 = lists:filtermap(fun({{file,modified},AbsPath,EventTS}=Event) ->
                                        case ?BLfile:read_file_info(AbsPath) of
                                            {ok,#file_info{mtime=MTime,size=Size}} ->
                                                OptsMap = (Fopts(AbsPath,EventTS))#{abs_path => AbsPath,
                                                    mtime => MTime,
                                                    size => Size},
                                                {true, {Event,OptsMap}};
                                            _ -> false
                                        end;
                                  ({{file,deleted},AbsPath,EventTS}=Event) ->
                                       {true, {Event,Fopts(AbsPath,EventTS)}};
                                  ({{file,{moved,FromPath}},AbsPath,EventTS}=Event) ->
                                       OptsMap = (Fopts(AbsPath,EventTS))#{from_rel_path => ?FldUtils:rel_path(RootDir,FromPath)},
                                       {true, {Event,OptsMap}};
                                  ({{file,attr},AbsPath,EventTS}=Event) ->
                                       case ?BLfile:read_file_info(AbsPath) of
                                           {ok,#file_info{mtime=MTime}} ->
                                               OptsMap = (Fopts(AbsPath,EventTS))#{mtime => MTime},
                                               {true, {Event,OptsMap}};
                                           _ -> false
                                       end;
                                  ({{dir,created},AbsPath,EventTS}=Event) ->
                                       case ?BLfile:read_file_info(AbsPath) of
                                           {ok,#file_info{mtime=MTime}} ->
                                               OptsMap = (Fopts(AbsPath,EventTS))#{mtime => MTime},
                                               {true, {Event,OptsMap}};
                                           _ -> false
                                       end;
                                  ({{dir,deleted},AbsPath,EventTS}=Event) ->
                                       {true, {Event,Fopts(AbsPath,EventTS)}};
                                  ({{dir,{moved,FromPath}},AbsPath,EventTS}=Event) ->
                                       OptsMap = (Fopts(AbsPath,EventTS))#{from_rel_path => ?FldUtils:rel_path(RootDir,FromPath)},
                                       {true, {Event,OptsMap}}
                                  % % DIR_MTIME: directory doesn't need mtime check
                                  %({{dir,attr},AbsPath,EventTS}=Event) ->
                                  %    case ?BLfile:read_file_info(AbsPath) of
                                  %        {ok,#file_info{mtime=MTime}} ->
                                  %            OptsMap = (Fopts(AbsPath,EventTS))#{mtime => MTime},
                                  %            {true, {Event,OptsMap}};
                                  %        _ -> false
                                  %    end
                               end, ToApply),
    ?LOG('$trace',"~ts. '~ts'. apply ~p fs events to nodes: ~120tp",[?APP,Code,length(ToApply1),SyncNodes]),
    EventInfo = {sync_changes,[node(),ToApply1]},
    Res = ?BLmulticall:call_direct(SyncNodes,{?FSRV,apply_sync_event,[Code,EventInfo]},5000),
    FailNodes = lists:foldl(fun({Node,undefined},Acc) -> [Node|Acc]; ({_,ok},Acc) -> Acc end, [], Res),
    State#fstate{sync_nodes=SyncNodes--FailNodes}.

%% @private
apply_changes_to_cache([],State) -> State;
apply_changes_to_cache(ToApply,State) ->
    lists:foldl(fun({{file,modified},AbsPath,EventTS},Acc) -> ?EventCache:cache_file_modified(AbsPath,EventTS,Acc);
                   ({{file,deleted},AbsPath,EventTS},Acc) -> ?EventCache:cache_file_deleted(AbsPath,EventTS,Acc);
                   ({{file,{moved,FromPath}},AbsPath,EventTS},Acc) -> ?EventCache:cache_file_moved(FromPath,AbsPath,EventTS,Acc);
                   ({{file,attr},AbsPath,EventTS},Acc) -> ?EventCache:cache_file_attr(AbsPath,EventTS,Acc);
                   ({{dir,created},AbsPath,EventTS},Acc) -> ?EventCache:cache_dir_created(AbsPath,EventTS,Acc);
                   ({{dir,deleted},AbsPath,EventTS},Acc) -> ?EventCache:cache_dir_deleted(AbsPath,EventTS,Acc);
                   ({{dir,{moved,FromPath}},AbsPath,EventTS},Acc) -> ?EventCache:cache_dir_moved(FromPath,AbsPath,EventTS,Acc)
                   %({{dir,attr},AbsPath,EventTS},Acc) -> ?EventCache:cache_dir_attr(AbsPath,EventTS,Acc) % DIR_MTIME: directory doesn't need mtime check
                end, State, ToApply),
    ?FldUtils:update_checksum(c,State).

%% -------------------------------------------------------
%% Routines
%% -------------------------------------------------------

%%%% --------
%%%% @private
%%check_in_cache(AbsPath,H1) ->
%%    case ?Dict:find(H1,AbsPath,undefined) of
%%        undefined -> false;
%%        Info ->
%%            case ?CacheBuilder:get_info(Info) of
%%                #file_info{type='directory'} -> true;
%%                _ -> false
%%            end end.
%%
%%%% --------
%%%% @private
%%check_in_portion(AbsPath,Events) ->
%%    lists:foldl(fun(_,true) -> true;
%%                   ({{dir,_},Path,_},_) -> Path==AbsPath;
%%                   ({{dir,{moved,Path}},_,_},_) -> Path==AbsPath;
%%                   (_,_) -> false
%%                end, false, Events).
%%
%%%% --------
%%%% @private
%%%% DIR_MTIME: directory doesn't need mtime check
%%check_attrs(AbsPath,H1) ->
%%    case ?Dict:find(H1,AbsPath,undefined) of
%%        undefined -> false;
%%        Info ->
%%            case ?BLfile:read_file_info(AbsPath) of
%%                {ok,#file_info{type=Type,mtime=MTime,size=Size}} ->
%%                    case ?CacheBuilder:get_info(Info) of
%%                        #file_info{type=Type,mtime=MTime,size=Size} -> true;
%%                        _ -> false
%%                    end;
%%                _ -> false
%%            end end.
