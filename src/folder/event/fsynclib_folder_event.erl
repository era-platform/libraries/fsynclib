%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.07.2021
%%% @doc Event routines
%%%     from file system - to queue for cooldown timeout,
%%%     from queue to cache and to other nodes,
%%%     from other nodes to file system and to cache
%%%     from file system when cache already modified - skip

-module(fsynclib_folder_event).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply_data/4]).

-export([apply_sync_event/3]).

-export([on_freeze_cancel/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("folder.hrl").

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% when file system changed
apply_data(Events,Dir,Filename,#fstate{cache_h1=_H1}=State) ->
    M = case Events of
            [<<"CREATE">>] -> {file,modified};
            [<<"CLOSE_WRITE">>,<<"CLOSE">>] -> {file,modified};
            [<<"DELETE">>] -> {file,deleted};
            [<<"CREATE">>,<<"ISDIR">>] -> {dir,created};
            [<<"DELETE">>,<<"ISDIR">>] -> {dir,deleted};
            [<<"MOVED_FROM">>] -> {file,moved_from};
            [<<"MOVED_TO">>] -> {file,moved_to};
            [<<"MOVED_FROM">>,<<"ISDIR">>] -> {dir,moved_from};
            [<<"MOVED_TO">>,<<"ISDIR">>] -> {dir,moved_to};
            [<<"ATTRIB">>] -> {file,attr};
            % [<<"ATTRIB">>,<<"ISDIR">>] -> {dir,attr}; % DIR_MTIME: directory doesn't need mtime check
            _ -> undefined
        end,
    case M of
        undefined -> State;
        _ -> enqueue(M,Dir,Filename,State)
    end.

%% when event from other node
apply_sync_event({FunName,Args},From,#fstate{code=Code}=State) ->
    try
        Res = case FunName of
                  sync_changes -> ?EventSync:sync_changes(Args,From,State);
                  _ -> {error,{invalid_params,?BU:strbin("Unknown function name: ~120tp",[FunName])}}
              end,
        case Res of
            {ok,State1} -> {reply,ok,State1};
            {error,_}=Err -> {reply,Err,State};
            {noreply,State1} -> {noreply,State1}
        end
    catch E:R:ST ->
        ?LOG('$crash',"~ts. '~ts'. sync from node crashed: {~120tp, ~120tp} \n~tStack: ~120tp",[?APP,Code,E,R,ST]),
        {reply,{error,R},State}
    end.

%% when freezing cancelled. Resume dequeue
on_freeze_cancel(#fstate{fs_queue=[]}=State) -> State;
on_freeze_cancel(State) ->
    FunDequeue = fun(StateF) -> dequeue(?BU:timestamp(), StateF) end,
    self() ! {apply,FunDequeue},
    State.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------------------------
%% Starting queue routines
%%   Modifies files:
%%      moved_from/moved_to into moved,
%%      moved_from into deleted,
%%      moved_to into modified
%%   Modifies files:
%%      moved_from/moved_to into moved,
%%      moved_from into deleted (pushing all sub items recursively),
%%      moved to into dir created and file modified for all sub items recursively
%% -------------------------------------------------------

%% enqueues for some time
enqueue(Event,Dir,Filename,#fstate{fs_queue=Q,code=Code}=State) ->
    NowTS = ?BU:timestamp(),
    AbsPath = build_path(Dir,Filename),
    FunDequeue = fun(StateF) -> dequeue(NowTS + ?TimeoutCoolDownQueue, StateF) end,
    _TimerRef = erlang:send_after(?TimeoutCoolDownQueue+20, self(), {apply,FunDequeue}),
    ?LOG('$trace',"~ts. '~ts'. fs event: ~120tp on '~ts~ts'. qlen: ~120tp",[?APP,Code,Event,Dir,Filename,length(Q)+1]),
    State#fstate{fs_queue=[{Event,AbsPath,NowTS} | Q]}.

%% dequeue timer
dequeue(_, #fstate{state=S}=State) when S/='$running' -> State;
dequeue(_, #fstate{fs_queue=[]}=State) -> State;
dequeue(Timestamp, #fstate{fs_last_dequeue_ts=LastDequeueTS}=State) when Timestamp =< LastDequeueTS+?TimeoutIdleQueue -> State;
dequeue(_, #fstate{fs_last_dequeue_ts=LastDequeueTS,fs_queue=Q}=State) ->
    case ?BU:timestamp() of
        NowTS when NowTS < LastDequeueTS + ?TimeoutIdleQueue -> State;
        NowTS ->
            QRev = lists:reverse(Q),
            ToApply = lists:takewhile(fun({_,_,TS}) -> NowTS >= TS+?TimeoutCoolDownQueue end, QRev),
            Q1 = lists:reverse(lists:nthtail(length(ToApply),QRev)),
            State1 = State#fstate{fs_queue=Q1,
                                  fs_last_dequeue_ts = NowTS},
            State2 = ?EventApply:apply(ToApply,State1),
            case ?TimeoutIdleQueue > 0 of
                false -> ok;
                true ->
                    FunDequeue = fun(StateF) -> dequeue(?BU:timestamp() + ?TimeoutIdleQueue + 1, StateF) end,
                    erlang:send_after(?TimeoutIdleQueue, self(), {apply,FunDequeue})
            end,
            State2
    end.

%% ====================================================================
%% Utils
%% ====================================================================

%% --------
%% @private
build_path(Dir,Filename) ->
    filename:join(?BU:to_unicode_list(Dir),?BU:to_unicode_list(Filename)).
